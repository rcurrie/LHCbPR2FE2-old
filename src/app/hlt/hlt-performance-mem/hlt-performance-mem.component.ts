import { Component, OnInit, ViewChild } from '@angular/core';

import { JSONQueryService } from '../../core/services/json-query/json-query.service';
import { PlotlyChartData } from '../../shared/components/plotly-chart/plotly-chart.component';
import { Subscriber } from 'rxjs';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { SimpleTableService, simpleCol } from '../simple-table/simple-table.component';
import { resultValues } from '../../core/services/json-query/json-query-interface';
import { SingleJobSelectorComponent } from '../../shared/components/single-job-selector/single-job-selector.component';

@Component({
  selector: 'app-mem-performance',
  templateUrl: './hlt-performance-mem.component.html'
})
export class PRHLTMemPerformance implements OnInit {

    perfData: resultValues[];
    percents: object = {};
    bytes: object = {};
    hasDataLoaded: boolean = false;
    progress: number = 0;
    svgPath: SafeResourceUrl;
    jobId: number = 0;
    appName: string = 'MOORE';
    execName: string = 'igprof-lb-run';

    @ViewChild('chart1') _chart1: PlotlyChartData;
    @ViewChild('chart2') _chart2: PlotlyChartData;

    tableCols1: simpleCol[];
    tableData1: object[];
    @ViewChild('table1') _table1: SimpleTableService;

    tableCols2: simpleCol[];
    tableData2: object[];
    @ViewChild('table2') _table2: SimpleTableService;

    @ViewChild('jobSelector') _selector: SingleJobSelectorComponent;

    constructor( private _queryService: JSONQueryService, private sanitizer: DomSanitizer ) {
    };

    /**
    * Returns the list of names of the top `wanted_entries` of parameters which have the largest `data_str` entry in `names_data`
    *
    */
    getTopSortedNames(names_data: object, data_str: string, wanted_entries: number, cutoff: number): string[] {
        var unsorted_data = [];

        for(var name in names_data) {
            const this_obj = {'name': name, 'value': names_data[name][data_str]};
            unsorted_data.push(this_obj);
        }

        var sorted_data = unsorted_data.sort( (a,b) => {
            return a['value'] - b['value'];
        });

        sorted_data = sorted_data.reverse();
        sorted_data = sorted_data.slice(0, wanted_entries);

        var sorted_names = [];
        for(var i = 0; i< sorted_data.length; i++) {
            if(sorted_data[i]['value'] > cutoff)
                sorted_names.push(sorted_data[i]['name']);
        }

        return sorted_names;
    };

    getTopSortedData(names_data: object, display_list: string[], data_str: string): number[] {
        var returnable=[];

        for(var i=0; i< display_list.length; i++) {
            returnable.push(names_data[display_list[i]][data_str]);
        }

        return returnable;
    };

    getOtherParams(names_data: object, data_str: string, display_list: string[], max: number): object[] {

        var other=0.;
        var total=0.;

        for(var name in names_data) {
            total = total + names_data[name][data_str];
            if(!(display_list.includes(name))) {
                other = other + names_data[name][data_str];
            }
        }

        var returnable = [{name: 'Other Methods', value: other},
                          {name: 'Unknown %-age', value: max-total}];

        console.log(returnable);

        return returnable;
    };

    getProgressSubscriber() : Subscriber<number> {
        return new Subscriber((progress) => {
            console.log("progress change");
            this.progress = progress;
        });
    };

    getSVGPath(job_id: number): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl('https://lblhcbpr.cern.ch/api/media/jobs/'+String(job_id)+'/perf.svg');
    }

    ngOnInit() {
    }

    performQuery(job_id: number) {
        this._queryService.getAllPerfData(String(job_id), this.getProgressSubscriber()).subscribe( returnedData => {
            if(returnedData.results.length != returnedData.count) console.log("ERROR IN REQUESTING DATA!");

            this.perfData = returnedData.results;
            for( var i=0; i< this.perfData.length; i++) {
                if( String(this.perfData[i]['name']).endsWith('_percent') ) {
                    if( this.perfData[i].description == "memory leak in %" ) {
                        this.percents[this.perfData[i]['name'].slice(0,-8)] =
                            {'mem_leak': parseFloat(this.perfData[i]['jobvalues'][0]['value']),
                             'leak_id': this.perfData[i].id };
                    }
                } else if( String(this.perfData[i]['name']).endsWith('_byte') ) {
                    if( this.perfData[i].description == "memory alloc in bytes" ) {
                        this.bytes[this.perfData[i]['name'].slice(0,-5)] = 
                            {'bytes': parseFloat(this.perfData[i]['jobvalues'][0]['value']),
                             'usage_id': this.perfData[i].id };
                    }
                }
            }
            console.log("PerfLen: " + this.perfData.length);

            console.log(this.percents);
            console.log(this.bytes);

            var labels = this.getTopSortedNames(this.percents, 'mem_leak', 20, 0.5);
            var values = this.getTopSortedData(this.percents, labels, 'mem_leak');
            var other_data = this.getOtherParams(this.percents, 'mem_leak', labels, 100.);

            labels.push(other_data[0]['name']);
            values.push(other_data[0]['value']);

            this._chart1.labels = labels.slice();
            this._chart1.data = values.slice();
            this._chart1.type = "pie";
            this._chart1.registerClickHandler(this.clickHandler);
            this._chart1.plot();

            var labels2 = this.getTopSortedNames(this.bytes, 'bytes', 20, 0.5);
            var values2 = this.getTopSortedData(this.bytes, labels2, 'bytes');
            var other_data2 = this.getOtherParams(this.bytes, 'bytes', labels2, 100.);

            labels2.push(other_data2[0]['name']);
            values2.push(other_data2[0]['value']);

            this._chart2.labels = labels2.slice();
            this._chart2.data = values2.slice();
            this._chart2.type = "pie";
            this._chart1.registerClickHandler(this.clickHandler);
            this._chart2.plot();

            this.hasDataLoaded = true;

            var memLeakName = new simpleCol();
            memLeakName.name = "Name";
            memLeakName.key = "name";
            var memLeakCol = new simpleCol();
            memLeakCol.name = 'Memory Leak in %';
            memLeakCol.key = 'leak';
            memLeakCol.trendable = true;
            memLeakCol.trend_key = 'leak_id';

            this.tableCols1 = [];
            this.tableCols1.push(memLeakName);
            this.tableCols1.push(memLeakCol);

            this.tableData1 = [];
            for( var name in this.percents ) {
                var thisRow1 = {"name": name,
                               "leak": this.percents[name]['mem_leak'],
                               "leak_id": this.percents[name]['leak_id']};
                this.tableData1.push(thisRow1);
            }

            var memUsageName = new simpleCol();
            memUsageName.name = "Name";
            memUsageName.key = "name";
            var memUsageCol = new simpleCol();
            memUsageCol.name = 'Memory Usage in bytes';
            memUsageCol.key = 'usage';
            memUsageCol.trendable = true;
            memUsageCol.trend_key = 'usage_id';

            this.tableCols2 = [];
            this.tableCols2.push(memUsageName);
            this.tableCols2.push(memUsageCol);

            this.tableData2 = [];
            for( var name in this.bytes ) {
                var thisRow2 = {"name": name,
                               "usage": this.bytes[name]['bytes'],
                               "usage_id": this.bytes[name]['usage_id']};
                this.tableData2.push(thisRow2);
            }


            this._table1.defineCols(this.tableCols1);
            this._table1.sendData(this.tableData1);
            this._table1.setComparisonJobs(this._selector.possibleJobs);

            this._table2.defineCols(this.tableCols2);
            this._table2.sendData(this.tableData2);
            this._table2.setComparisonJobs(this._selector.possibleJobs);
        });
    };

    clickHandler(data) {
        console.log(data);
        console.log("Mem click handler");
    };

    jobChanged(newJobID: number) {
        if(newJobID) this.performQuery(newJobID);
        this.jobId = newJobID;
    };

}
