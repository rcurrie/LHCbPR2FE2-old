import { Component, Input, Output, EventEmitter, OnInit, ViewChild, ElementRef } from '@angular/core';

import { JSONFormatRule } from '../json-table/json-format.component';
import { JSONQueryService } from '../../core/services/json-query/json-query.service';

import { HLTCompareValuesComponent } from '../compare-values/compare-values.component';
import { JSONFilterTable, FilterButton } from '../json-filter-table/json-filter-table.component';
import { SingleJobSelectorComponent } from '../../shared/components/single-job-selector/single-job-selector.component';

@Component({
  selector: 'app-hlt-timingtest',
  templateUrl: './hlt-timingtest.component.html',
  providers: [  JSONFilterTable,
                HLTCompareValuesComponent,
              ],
})
export class PRHLTimingTest implements OnInit {

  users = [];
  value = {};
  value2 = {};
  testParam: string;
  @ViewChild('filterTable') _tableService: JSONFilterTable;
  @ViewChild('jobSelector') _selector: SingleJobSelectorComponent;
  public jobId: number;
  mooreOnlineName: string="MOOREONLINE";

  hlt1perfCols = {  'Name':            {'filterable': true, 'key': 'name', 'trendable': false, 'sortCol': 'name', 'hideable': false},
                  'Total (s)':         {'key': 'total (s)_str', 'sortCol': 'total (s)_value' },
                  'User':              {'key': '<user>_str', 'sortCol': '<user>_value'},
                  'System':            {'key': '<clock>_str', 'precision': 1, 'sortCol': '<clock>_value'},
                  'Children':           {'key': '_str', 'precision': 1, 'sortCol': ''} };
  hlt1perfColOrder = ['Name', 'Total (s)', 'User', 'System', 'Parents'];

  hlt2perfCols = this.hlt1perfCols;
  hlt2perfColsOrder = this.hlt1perfColOrder;

  filter_buttons = [
      {'name': 'HLT1 PerfTest', 'contains': ['HLTPerfHlt1_',], 'not_contains': ['throughput'], 'cols': this.hlt1perfCols},
      {'name': 'HLT2 PerfTest', 'contains': ['HLTPerfHlt2_',], 'not_contains': ['throughput'], 'cols': this.hlt2perfCols},
  ];

  constructor(private _queryService: JSONQueryService) {
  };

  ngOnInit() {

    var buttons = [];
    for(var i=0; i< this.filter_buttons.length; i++ ) {
        var newButton = new FilterButton();
        newButton.name = this.filter_buttons[i].name;
        newButton.contains = this.filter_buttons[i].contains;
        if('not_contains' in this.filter_buttons[i]) {
            newButton.not_contains = this.filter_buttons[i].not_contains;
        }
        var tableFormat = new JSONFormatRule();
        tableFormat.addColumns(this.filter_buttons[i].cols);
        newButton.columns = tableFormat;
        buttons.push(newButton);
    }

    this._tableService.setUpTable(buttons);
  };

  jobChanged(newJobID: number) {
    this.jobId = newJobID;
    this._tableService.jobChanged( this.jobId, this._selector.fixedSelectionState );
  };

};
