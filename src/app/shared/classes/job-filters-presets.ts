export class PRJobFiltersPresets {
  app_id = 0;
  options_ids: number[] = [];
  checked_options_ids: number[] = [];

  constructor(apps = 0, options = [], checked = []) {
    this.app_id = apps;
    this.options_ids = options;
    this.checked_options_ids = checked;
  }
}
