import { PRIBasicApiResponse } from '../interfaces/basic-api-response';
import { PRIVersionsApiResponse } from '../interfaces/versions-api-response';
import { PRJobsDataResponse } from '../classes/jobs-data-response';
import { PRIJobsComparisonParams } from '../interfaces/jobs-comparison-params';
import { PRIJobsSearchParams } from '../interfaces/jobs-search-params';

export class PRSelectionState {
  app_id: number;

  with_nightly: boolean;
  check_specific: boolean;
  nightly_version_number: number;

  unfiltered_versions: PRIVersionsApiResponse[];
  filtered_versions: number[];

  unfiltered_options: PRIBasicApiResponse[];
  filtered_options: number[];

  unfiltered_executables: PRIBasicApiResponse[];
  filtered_executables: number[];

  unfiltered_hosts: PRIBasicApiResponse[];
  filtered_hosts: number[];

  unfiltered_platforms: PRIBasicApiResponse[];
  filtered_platforms: number[];

  jobsdata_layout_page_size: number;
  jobsdata_layout_page_sizes: number[];

  jobs_data: PRJobsDataResponse;

  constructor() {
    this.jobsdata_layout_page_sizes = [10, 15, 20, 50, 100];
    this.jobsdata_layout_page_size = this.jobsdata_layout_page_sizes[0];

    this.resetAll();
  }

  resetAll() {
    this.app_id = 0;

    this.with_nightly = false;
    this.check_specific = false;
    this.nightly_version_number = 10;

    this.jobs_data = new PRJobsDataResponse();

    this.resetUnfiltered();
    this.resetFiltered();
  }

  resetUnfiltered() {
    this.unfiltered_versions = [];
    this.unfiltered_options = [];
    this.unfiltered_executables = [];
    this.unfiltered_hosts = [];
    this.unfiltered_platforms = [];
  }

  resetFiltered() {
    this.filtered_versions = [];
    this.filtered_options = [];
    this.filtered_executables = [];
    this.filtered_hosts = [];
    this.filtered_platforms = [];
  }

  getSearchJobsHttpParams(params?: PRIJobsSearchParams): {} {
    const outparams: any = {};

    outparams.application = params.app_id ? String(params.app_id) : String(this.app_id);

    if (params.versions) {
      outparams.versions = String(params.versions.join(','));
    } else if (this.filtered_versions.length) {
      outparams.versions = String(this.filtered_versions.join(','));
    }

    if (params.options) {
      outparams.options = String(params.options.join(','));
    } else if (this.filtered_options.length) {
      outparams.options = String(this.filtered_options.join(','));
    }

    if (params.executables) {
      outparams.executables = String(params.executables.join(','));
    } else if (this.filtered_executables.length) {
      outparams.executables = String(this.filtered_executables.join(','));
    }

    if (params.platforms) {
      outparams.platforms = String(params.platforms.join(','));
    } else if (this.filtered_platforms.length) {
      outparams.platforms = String(this.filtered_platforms.join(','));
    }

    if (params.hosts) {
      outparams.hosts = String(params.hosts.join(','));
    } else if (this.filtered_hosts.length) {
      outparams.hosts = String(this.filtered_hosts.join(','));
    }

    if (params.pagesize) {
      outparams.page_size = params.pagesize;
    } else {
      if (this.jobsdata_layout_page_size !== this.jobsdata_layout_page_sizes[0]) {
        outparams.page_size = this.jobsdata_layout_page_size;
      }
    }

    if (params.page) {
      outparams.page = String(params.page);
    }

    return(outparams);
  }

  getCompareJobsHttpParams(params: PRIJobsComparisonParams): {} {
    const outparams: any = {};

    if (params.ids) {
      outparams.ids = String(params.ids.join(','));
    }

    if (params.pagesize) {
      outparams.page_size = params.pagesize;
    } else {
      if (this.jobsdata_layout_page_size !== this.jobsdata_layout_page_sizes[0]) {
        outparams.page_size = this.jobsdata_layout_page_size;
      }
    }

    if (params.page) {
      outparams.page = String(params.page);
    }

    if (params.filterby) {
      outparams.contains = String(params.filterby);
    }

    if (params.type) {
      outparams.type = String(params.type);
    }

    return(outparams);
  }
}
