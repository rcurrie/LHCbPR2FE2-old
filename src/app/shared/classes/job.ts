export class PRJob {
  'id': number;
  'resource_uri': string;
  'job_description': {
    'application_version': {
      'id': number;
      'application': {
        'id': number;
        'name': string;
      };
      'version': string;
      'is_nightly': boolean;
      'job_descriptions': [
        {
          'setup_project': any;
          'option': {
            'id': number;
            'content': string;
            'description': string;
            'thresholds': any[];
          };
          'executable': {
            'id': number;
            'name': string;
            'content': string;
          };
        }
      ];
    };
    'setup_project': any;
    'option': {
      'id': number;
      'content': string;
      'description': string;
      'thresholds': any[];
    };
  };
  'host': {
    'id': number;
    'hostname': string;
    'cpu_info': string;
    'memory_info': string;
  };
  'platform': {
    'content': string;
  };
  'time_start': string;
  'time_end': string;
  'status': string;
  'is_success': boolean;
}
