import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PRGeantRoutingModule } from './geant.routing.module';
import { PRSharedModule } from '../shared/shared.module';

import { PRGeantComponent } from './geant.component';

@NgModule({
  imports: [
    CommonModule,
    PRGeantRoutingModule,
    PRSharedModule
  ],
  declarations: [
    PRGeantComponent
  ],
  exports: [
    PRGeantComponent
  ]
})
export class PRGeantModule { }
