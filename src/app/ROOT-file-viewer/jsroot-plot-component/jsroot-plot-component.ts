import { Component, Input, OnInit, AfterViewInit } from '@angular/core';

import { PRPlotDisplayOpts } from '../../core/shared/plot-display-opts';

declare var JSROOT: any;

@Component({
  selector: 'app-jsroot-plot',
  templateUrl: 'jsroot-plot-component.html'
})
export class PRJSROOTPlotComponent implements OnInit, AfterViewInit {
  @Input() public items: any[] = [];
  @Input() public id = 0;
  @Input() public displayOpt = PRPlotDisplayOpts.superimposed;

  public displayOpts = PRPlotDisplayOpts;
  public containers_ids: string[] = [];
  public plottable = true;
  // public container_height = 400;
  private id_prefix = 'rfv-jsroot-plot-container-';
  private n_plots = 0;

  ngOnInit() {
    // Prepare containers for plots
    this.n_plots = 0;
    this.items.forEach(plot_item => {
      if (plot_item['job_id']) {
        this.n_plots++;
      }
    });

    // if (this.n_plots === 2) {
    //   this.container_height = 300;
    // } else if (this.n_plots === 3) {
    //   this.container_height = 250;
    // }

    if (this.n_plots) {
      if (this.n_plots < 4 && this.displayOpt === PRPlotDisplayOpts.split) {
        for (let i = 0; i < this.n_plots; i++) {
          this.containers_ids.push(this.id_prefix + String(this.id) + '-' + String(i));
        }
      } else {
        this.containers_ids.push(this.id_prefix + String(this.id));
      }
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.displayOpt === this.displayOpts.split) {
        if (this.n_plots < 4) {
          this.items.forEach((plot, index) => {
            document.getElementById(this.containers_ids[index] + '-title').innerHTML = 'JobID: ' + plot['job_id'];
            JSROOT.redraw(this.containers_ids[index], JSROOT.JSONR_unref(plot.plot_data));
          });
        } else {
          this.plottable = false;
        }
      } else {
        if (this.n_plots === 1) {
          this.plottable = false;
        } else {
          let ks_result = 'Kolmogorov-Smirnov test result: ';

          if (document.getElementById(this.containers_ids[0] + '-title')) {
            document.getElementById(this.containers_ids[0] + '-title').innerHTML = 'JobID-s:';
          }

          this.items.forEach((plot) => {
            if (plot['job_id']) {
              document.getElementById(this.containers_ids[0] + '-title').innerHTML += ' ' + plot['job_id'];
              JSROOT.draw(this.containers_ids[0], JSROOT.JSONR_unref(plot.plot_data), 'same');
            } else {
              if (this.displayOpt !== this.displayOpts.kolmogorov) {
                JSROOT.draw(this.containers_ids[0], JSROOT.JSONR_unref(plot.plot_data), 'same');
              } else {
                ks_result += plot['plot_data'];
              }
            }
          });

          if (this.displayOpt === this.displayOpts.kolmogorov && this.n_plots > 1) {
            document.getElementById(this.containers_ids[0] + '-title').innerHTML += '<br>' + ks_result;
          }
        }
      }
    });
  }
}
