import { Component, OnInit, ViewChild } from '@angular/core';

import { JSONTableService } from './json-table/json-table.component';
import { JSONFormatRule } from './json-table/json-format.component';
import { PlotlyChartData } from '../shared/components/plotly-chart/plotly-chart.component';

import { HLTCompareValuesComponent } from './compare-values/compare-values.component';

@Component({
  selector: 'app-hlt',
  templateUrl: './hlt.component.html',
  providers: [  JSONTableService,
                HLTCompareValuesComponent,
              ],
})
export class PRHLTComponent implements OnInit {

  users = [];
  value = {};
  value2 = {};
  testParam: string;
  @ViewChild('tableData') _tableService: JSONTableService;
  @ViewChild('tableData2') _tableService2: JSONTableService;
  @ViewChild('chart1') _chart1: PlotlyChartData;
  @ViewChild('chart2') _chart2: PlotlyChartData;
  public jobId: number=0;
  mooreName: string="MOORE";

  table_columns = { 'Name':           {'filterable': true, 'key': 'name', 'trendable': false, 'sortCol': 'name'},
                    'Rate':           {'sortable': true, 'hideable': true, 'key': 'rate_str', 'sortCol': 'rate' },
                    'Rate Unique':    {'hideable': true, 'key': 'rate_unique_str', 'sortCol': 'rate_unique'},
                    'MBs':            {'hideable': true, 'key': 'MBs_incl_str', 'precision': 1, 'sortCol': 'MBs_incl'},
                    'kBe':            {'hideable': true, 'key': 'kBe_incl_str', 'precision': 1, 'sortCol': 'kBe_incl'} };

  constructor() {};

  ngOnInit() {
    
    var tableFormat = new JSONFormatRule();
    tableFormat.stripName = 'hlt1_Hlt1Lines_';
    tableFormat.addColumns(this.table_columns);

    this._tableService.setupTable(tableFormat);
  };

  jobChanged(newJobID: number) {
    console.log("Changes in Parent " + newJobID);
    this.jobId = newJobID;
    this._tableService.inputJob = this.jobId;
  }

};
