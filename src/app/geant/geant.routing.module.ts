import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PRGeantComponent } from './geant.component';

export const routes: Routes = [
  { path: 'hadronic', component: PRGeantComponent },
  { path: 'calo', component: PRGeantComponent },
  { path: 'msc', component: PRGeantComponent },
  { path: 'richtb', component: PRGeantComponent },
  { path: '', redirectTo: 'hadronic', pathMatch: 'full' },
  { path: '**', redirectTo: 'hadronic' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PRGeantRoutingModule { }
