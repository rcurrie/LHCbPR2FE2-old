import { Component, Input, Output, EventEmitter, OnInit, ViewChild, ElementRef } from '@angular/core';

import { JSONFormatRule } from '../json-table/json-format.component';
import { JSONQueryService } from '../../core/services/json-query/json-query.service';

import { HLTCompareValuesComponent } from '../compare-values/compare-values.component';
import { JSONFilterTable, FilterButton } from '../json-filter-table/json-filter-table.component';
import { SingleJobSelectorComponent } from '../../shared/components/single-job-selector/single-job-selector.component';

@Component({
  selector: 'app-hlt-ratetest',
  templateUrl: './hlt-ratetest.component.html',
  providers: [  JSONFilterTable,
                HLTCompareValuesComponent,
              ],
})
export class PRHLTRateTest implements OnInit {

  users = [];
  value = {};
  value2 = {};
  testParam: string;
  @ViewChild('filterTable') _tableService: JSONFilterTable;
  @ViewChild('jobSelector') _selector: SingleJobSelectorComponent;
  public jobId: number;
  mooreName: string="MOORE";

  hlt1Cols = {  'Name':           {'filterable': true, 'key': 'name', 'trendable': false, 'sortCol': 'name', 'hideable': false},
                'Rate':           {'key': 'rate_str', 'sortCol': 'rate' },
                'Rate Unique':    {'key': 'rate_unique_str', 'sortCol': 'rate_unique'},
                'MBs':            {'key': 'MBs_incl_str', 'precision': 1, 'sortCol': 'MBs_incl'},
                'kBe':            {'key': 'kBe_incl_str', 'precision': 1, 'sortCol': 'kBe_incl'} };
  hlt1ColOrder = ['Name', 'Rate', 'Rate Unique', 'MBs', 'kBe'];

  regexCols = {
    'Name': {'key': 'name', 'trendable': false, 'hideable': false, 'sortCol': 'name'},
    'Inclusive Rate': {'key': 'rate_inclusive_str', 'sortCol': 'rate_inclusive'},
    'Inclusive MBs':  {'key': 'MBs_inclusive_str', 'sortCol': 'MBs_inclusive'},
    'Inclusive kBe':  {'key': 'kBe_inclusive_str', 'sortCol': 'kBe_inclusive'},
    'Exclusive Rate': {'key': 'rate_exclusive_str', 'sortCol': 'rate_exclusive'},
    'Exclusive MBs':  {'key': 'MBs_exclusive_str', 'sortCol': 'MBs_exclusive'},
    'Exclusive kBe':  {'key': 'kBe_exclusive_str', 'sortCol': 'kBe_exclusive'},  };
  regexColOrder = ['Name', 'Inclusive Rate', 'Inclusive MBs', 'Inclusive kBe', 'Exclusive Rate', 'Exclusive MBs', 'Exclusive kBe'];

  hlt2overlapCols = {
    'Name': {'key': 'name', 'trendable': false, 'hideable': false, 'sortCol': 'name'},
    'Overlap': {'key': 'rate_TurboAndFull_str', 'custom_val': 'rate_TurboAndFull', 'custom_err': 'rate_err_TurboAndFull', 'sortCol': 'rate_TurboAndFull'},
    'Total': {'key': 'rate_str', 'sortCol': 'rate' },
  };
  hlt2overlapColOrder = ['Name', 'Overlap', 'Total'];


  hlt2fullCols = {
    'Name': {'key': 'name', 'trendable': false, 'hideable': false, 'sortCol': 'name'},
    'Rate': {'key': 'rate_str', 'sortCol': 'rate'},
    'Rate Unique': {'key': 'rate_unique_str', 'sortCol': 'rate_unique' },
    'MBs': {'key': 'MBs_incl_str', 'sortCol': 'MBs_incl'},
    'kBe': {'key': 'kBe_incl_str', 'sortCol': 'kBe_incl'},
    'persist': {'key': 'persistReco_str', 'sortCol': 'persistReco'}
  };
  hlt2fullColOrder = ['Name', 'Rate', 'Rate Unique', 'MBs', 'kBe', 'persist'];


  hlt2turboCols = {
    'Name': {'key': 'name', 'trendable': false, 'hideable': false, 'sortCol': 'name'},
    'Rate': {'key': 'rate_str', 'sortCol': 'rate'},
    'Rate Unique': {'key': 'rate_unique_str', 'sortCol': 'rate_unique' },
    'MBs': {'key': 'MBs_incl_str', 'sortCol': 'MBs_incl'},
    'kBe': {'key': 'kBe_incl_str', 'sortCol': 'kBe_incl'},
    'persist': {'key': 'persistReco_str', 'sortCol': 'persistReco'}
  };
  hlt2turboColOrder = ['Name', 'Rate', 'Rate Unique', 'MBs', 'kBe', 'persist'];


  hlt2turboCalibCols = {
    'Name': {'key': 'name', 'trendable': false, 'hideable': false, 'sortCol': 'name'},
    'Rate': {'key': 'rate_str', 'sortCol': 'rate'},
    'Rate Unique': {'key': 'rate_unique_str', 'sortCol': 'rate_unique' },
    'MBs': {'key': 'MBs_incl_str', 'sortCol': 'MBs_incl'},
    'kBe': {'key': 'kBe_incl_str', 'sortCol': 'kBe_incl'},
    'persist': {'key': 'persistReco_str', 'sortCol': 'persistReco'}
  };
  hlt2turboCalibColOrder = ['Name', 'Rate', 'Rate Unique', 'MBs', 'kBe', 'persist'];

  hlt2totalCols = {
    'Name': {'key': 'name', 'trendable': false, 'hideable': false, 'sortCol': 'name'},
    'Total Rate': {'key': 'rate_str', 'sortCol': 'rate'},
    'Total MBs': {'key': 'MBs_incl_str', 'sortCol': 'MBs_incl' },
  };
  hlt2totalColOrder = ['Name', 'Total Rate', 'Total MBs'];

  hlt2matrixCols = {
    'Name': {'key': 'name', 'trendable': false, 'hideable': false, 'sortCol': 'name'},
    'CharmFull': {'key':'CharmFull_str', 'sortCol': 'CharmFull'},
    'CharmTurbo': {'key': 'CharmTurbo_str', 'sortCol': 'CharmTurbo'},
    'EW': {'key': 'EW_str', 'sortCol': 'EW'},
    'Hlt2Technical': {'key': 'Hlt2Technical_str', 'sortCol': 'Hlt2Technical'},
    'Leptons': {'key': 'Leptons_str', 'sortCol': 'Leptons'},
    'LowMult': {'key': 'LowMult_str', 'sortCol': 'LowMult'},
    'Other': {'key': 'Other_str', 'sortCol': 'Other'},
    'Topo': {'key': 'Topo_str', 'sortCol': 'Topo'},
    'TurboCalib': {'key': 'TurboCalib_str', 'sortCol': 'TurboCalib'}
  };
  hlt2matrixColsOrder = ['CharmFull', 'CharmTurbo', 'EW', 'Hlt2Technical', 'Leptons', 'LowMult', 'Other', 'Topo', 'TurboCalib'];

  hlt2rateByStreamCols = {
    'Name': {'key': 'name', 'trendable': false, 'hideable': false, 'sortCol': 'name'},
    'Rate': {'key': 'rate_str', 'sortCol': 'rate'},
    'Rate Unique': {'key': 'rate_unique_str', 'sortCol': 'rate_unique'},
    'Rate Full': {'key': 'rate_Full_str', 'sortCol': 'rate_Full'},
    'Rate Turbo': {'key': 'rate_Turbo_str', 'sortCol': 'rate_Turbo'},
    'Rate TurboCalib': {'key': 'rate_TurboCalib_str', 'sortCol': 'rate_TurboCalib'},
  };
  hlt2rateByStreamColsOrder = ['Rate', 'Rate Unique', 'Rate Full', 'Rate Turbo', 'Rate TurboCalib'];

  hlt2rateAllStreamCols = {
    'Name': {'key': 'name', 'trendable': false, 'hideable': false, 'sortCol': 'name'},
    'Rate': {'key': 'rate_str', 'sortCol': 'rate'},
    'Rate Unique': {'key': 'rate_unique_str', 'sortCol': 'rate_unique'},
    'Rate Full': {'key': 'rate_Full_str', 'sortCol': 'rate_Full'},
    'Rate Turbo': {'key': 'rate_Turbo_str', 'sortCol': 'rate_Turbo'},
    'Rate TurboCalib': {'key': 'rate_TurboCalib_str', 'sortCol': 'rate_TurboCalib'},
  };
  hlt2rateAllStreamColsOrder = ['Rate', 'Rate Unique', 'Rate Full', 'Rate Turbo', 'Rate TurboCalib'];

  filter_buttons = [
      {'name': 'HLT1 Lines', 'contains': ['hlt1_Hlt1Lines_',], 'cols': this.hlt1Cols},
      {'name': 'HLT1ByRegex', 'contains': ['hlt1_ByRegex_',], 'cols': this.regexCols},
      {'name': 'Hlt2 Overlap TurboAndFull', 'contains': ['hlt2_decisions_'], 'cols': this.hlt2overlapCols},
      {'name': 'Hlt2 Rates Full', 'contains': ['hlt2_decisions_'], 'not_contains': ['Total', 'TurboCalib'], 'cols': this.hlt2fullCols},
      {'name': 'Hlt2 Rates Turbo', 'contains': ['hlt2_decisions_', 'Turbo'], 'not_contains': ['TurboCalib', 'Total'], 'cols': this.hlt2turboCols},
      {'name': 'Hlt2 Rates TurboCalib', 'contains': ['hlt2_decisions_', 'TurboCalib'], 'not_contains': ['Total'], 'cols': this.hlt2turboCalibCols},
      {'name': 'Hlt2Rates Total', 'contains': ['hlt2_decisions_', 'Total'], 'not_contains': [], 'cols': this.hlt2totalCols},
      {'name': 'RateMatrix', 'contains': ['hlt2_RateMatrixDict_'], 'not_contains': ['All', 'lines'], 'cols': this.hlt2matrixCols},
      {'name': 'RatesByStream AllStreams', 'contains': ['hlt2_Streams_AllStreams_'], 'not_contains': [], 'cols': this.hlt2rateAllStreamCols},
      {'name': 'RatesByStream Modules', 'contains': ['hlt2_Streams_Modules_'], 'not_contains': [], 'cols': this.hlt2rateByStreamCols}
  ];

  constructor(private _queryService: JSONQueryService) {
  };

  ngOnInit() {

    var buttons = [];
    for(var i=0; i< this.filter_buttons.length; i++ ) {
        var newButton = new FilterButton();
        newButton.name = this.filter_buttons[i].name;
        newButton.contains = this.filter_buttons[i].contains;
        if('not_contains' in this.filter_buttons[i]) {
            newButton.not_contains = this.filter_buttons[i].not_contains;
        }
        var tableFormat = new JSONFormatRule();
        tableFormat.addColumns(this.filter_buttons[i].cols);
        newButton.columns = tableFormat;
        buttons.push(newButton);
    }

    this._tableService.setUpTable(buttons);
  };

  jobChanged(newJobID: number) {
    this.jobId = newJobID;
    this._tableService.jobChanged( this.jobId, this._selector.fixedSelectionState );
  };

};
