import { Component, Input, ViewChild, Output } from '@angular/core';

import { JSONQueryService } from '../../../core/services/json-query/json-query.service';

@Component({
    selector: 'plotly-chart-data',
    templateUrl: './plotly-chart.component.html'
  })
export class PlotlyChartData {
    @Input() public data: number[]=[];
    @Input() public error: number[]=[];
    @Input() public pulls: number[]=[];
    @Input() public labels: string[]=[];
    @Input() public type: string="error";
    @Input() public title: string='';
    @Input() public appName: string='';

    private hiddenAxis = { "title": "",
      "zeroline": false,
      "showline": false,
      "showticklabels": false,
      "showgrid": false };

    @Output() public _dynamic: boolean = true;
    @Output() public  _plotData: object[] = [];
    @Output() public _plotLayout: object = {"title": "", "xaxis": this.hiddenAxis, "yaxis": this.hiddenAxis};
    @Output() public _plotClick: any;
    @Output() public _config: object = {"displayModeBar": false};

    hasPlotted: boolean = false;
    plotData: object[] = [];
    g_layout: object={};
    userHandlers: Function[] = [];
    @ViewChild('chartObj') chart: any;

    constructor(private _queryService: JSONQueryService) {};

    plot() {
      if( this.type == "pie" ){
        if(this.pulls.length < this.data.length) {
          for( var i=this.pulls.length; i< this.data.length; i++ ) {
            this.pulls.push(0.);
          }
        }
        this.plotData = [{
          values: this.data,
          labels: this.labels,
          pull: this.pulls,
          type: 'pie',
          layout: {
            autosize: true,
            displaylogo: false
          },
          direction: "clockwise",
          rotation: 315,
        }];

        this.g_layout = {
          title: this.title,
          displaylogo: false,
          xaxis: {
            title: "pie Chart",
            categoryorder: "array",
            categoryarray:  this.labels
          },
          autosize: false,
          height: 1024,
          legend: {
            orientation: 'h',
          }          
        };
      } else {
        this.plotData = [{
          x: this.labels,
          y: this.data,
          error_y: {type: 'data', array: this.error, visible: true},
          layout: {
            autosize: true,
            displaylogo: false
          }
        }];

        this.g_layout = {
          title: this.title,
          autosize: true,
          displaylogo: false,
          margin: {
            l: 50,
            r: 50,
            b: 50,
            t: 50,
            pad: 4
          },
        };
      }

      this.chart.click.subscribe( function(data) {
        this.clickHandler(data);
      }.bind(this));

      this._plotData = this.plotData;
      this._plotLayout = this.g_layout;
      this._config = {};

    };

    clickHandler(data) {
      for( var i=0; i< this.userHandlers.length; i++ ) {
        this.userHandlers[i](data);
      }
    };

    registerClickHandler(someFunction) {
      this.userHandlers.push(someFunction);
    };

    sortByDate() {
      var newData=[];
      var newError=[];
      var newLabel=[];
      var numberList=[];
      var totalDict={};
      for( var i=0; i< this.labels.length; i++ ) {
        var number = new Date(this.labels[i]).getTime();
        totalDict[number] = {
          'data': this.data[i],
          'error': this.error[i],
          'label': this.labels[i]
        };
        numberList.push(number);
      }
      numberList.sort();
      for( var i=0; i< numberList.length; i++ ) {
        newData.push(totalDict[numberList[i]].data);
        newError.push(totalDict[numberList[i]].error);
        newLabel.push(totalDict[numberList[i]].label);
      }

      this.data = newData.slice();
      this.error = newError.slice();
      this.labels = newLabel.slice();
    };

    plotQuery(attrib: string, jsonObj: string, app_version: string, limit: number=21, otherArgs: string[]=[], clickHandler: Function=undefined) {
      this._queryService.getJSONTrendData(attrib, jsonObj, app_version, limit, otherArgs).subscribe( JSONTrendData => {

        var plottableData = [];
        var plottableError = [];
        var plottableLabels = [];
        for( const data_i in JSONTrendData['results'] ) {
          var data_t = JSONTrendData['results'][data_i];
          if(data_t[attrib] instanceof Object ) {
            plottableData.push(data_t[attrib]['average']);
            plottableError.push(data_t[attrib]['error']);
            plottableLabels.push(data_t['date']);            
          } else {
            plottableData.push(data_t[attrib]);
            plottableError.push(data_t[attrib+'_err']);
            plottableLabels.push(data_t['date']);
          }
        }

        this.data = plottableData;
        this.error = plottableError;
        this.labels = plottableLabels;

        this.sortByDate();
        this.registerClickHandler(clickHandler);
        this.plot();
      });
    };

};
