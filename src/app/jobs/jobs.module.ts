import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClarityModule } from '@clr/angular';

import { PRJobsRoutingModule } from './jobs.routing.module';
import { PRSharedModule } from '../shared/shared.module';
import { PRSearchJobsModule } from '../search-jobs/search-jobs.module';
import { PRJobsFileViewerModule } from './jobs-file-viewer/jobs-file-viewer.module';

import { PRJobsComponent } from './jobs.component';
import { PRSelectValuesComponent } from './select-values/select-values.component';
import { PRCompareValuesComponent } from './compare-values/compare-values.component';
import { PRCompareValuesChartComponent } from './compare-values-chart/compare-values-chart.component';

import { ChartsModule } from 'ng4-charts/ng4-charts';

@NgModule({
  imports: [
    CommonModule,
    ClarityModule,
    ChartsModule,
    PRJobsRoutingModule,
    PRSharedModule,
    PRSearchJobsModule,
    PRJobsFileViewerModule
  ],
  declarations: [
    PRJobsComponent,
    PRSelectValuesComponent,
    PRCompareValuesComponent,
    PRCompareValuesChartComponent
  ]
})
export class PRJobsModule { }
