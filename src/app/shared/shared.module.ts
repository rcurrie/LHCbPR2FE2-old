import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PRBreakLongStringPipe } from '../shared/pipes/break-long-string.pipe';
import { PRPageSizeSelectorComponent } from './components/page-size-selector/page-size-selector.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PRBreakLongStringPipe,
    PRPageSizeSelectorComponent
  ],
  exports: [
    PRBreakLongStringPipe,
    PRPageSizeSelectorComponent
  ]
})
export class PRSharedModule { }
