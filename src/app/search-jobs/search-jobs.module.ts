import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { ClarityModule } from '@clr/angular';

import { PRSharedModule } from '../shared/shared.module';

import { PRSearchJobsComponent } from './search-jobs.component';
import { PRBasicFilterComponent } from './filter-components/basic-filter.component';
import { PRVesionsFilterComponent } from './filter-components/versions-filter.component';
import { PRSelectJobsComponent } from './select-jobs/select-jobs.component';

@NgModule({
  imports: [
    CommonModule,
    ClarityModule,
    HttpClientModule,
    ReactiveFormsModule,
    PRSharedModule
  ],
  declarations: [
    PRSearchJobsComponent,
    PRBasicFilterComponent,
    PRVesionsFilterComponent,
    PRSelectJobsComponent
  ],
  exports: [
    PRSearchJobsComponent,
    PRSelectJobsComponent
  ]
})
export class PRSearchJobsModule { }
