export interface PRIJobsComparisonParams {
  ids: number[];
  pagesize?: number;
  page?: number;
  filterby?: string;
  contains?: string;
  type?: string;
}
