
import { Component, OnInit, ViewChild } from '@angular/core';

import { HLTCompareValuesComponent } from '../compare-values/compare-values.component';
import { JSONQueryService } from '../../core/services/json-query/json-query.service';

export class simpleCol {
    name: string='';
    key: string='';
    trend_key: string='';
    hideable: boolean=false;
    trendable: boolean=false;
}

@Component({
  selector: 'app-simple-table',
  templateUrl: './simple-table.component.html'
})
export class SimpleTableService implements OnInit {

    @ViewChild(HLTCompareValuesComponent) compareValuesChild: HLTCompareValuesComponent;
    @ViewChild('thisDataGrid') thisDataGrid: any;

    viewableRows: object[] = [];
    viewableCols: object[] = [];
    comparisonJobs: object[] = [];
    loading: boolean=false;
    pageSize: number=10;
    pageSizes: number[] = [10,25,50,100];

    constructor(private _queryService: JSONQueryService) {
    };

    ngOnInit() {

    };

    clearTable() {
        this.viewableCols = [];
    };

    defineCols(colStructure: simpleCol[]) {
        this.viewableCols = colStructure;
    };

    sendData(inputData: object[]) {
        this.viewableRows = inputData;
    };

    setComparisonJobs(jobs: object[]) {
        this.comparisonJobs = jobs;
    };

    makeTrend(thisCol, thisRow) {

        var attrId = thisRow[thisCol.trend_key];

        var jobIds=''
        for( var i=0; i< this.comparisonJobs.length; i++ ) {
            if( i == 21 ) break;
            jobIds = jobIds + String(this.comparisonJobs[i]['id'])+',';
        }
        jobIds=jobIds.slice(0,-1);

        this._queryService.getPerfData(jobIds, 1, attrId).subscribe( returnedData => {

            var values = [];
            var dates = [];
            for( var i=0; i< returnedData.results.length; i++ ) {
                var thisResult = returnedData.results[i];
                for( var j=0; j< returnedData.results[i].jobvalues.length; j++ ) {
                    values.push(returnedData.results[i].jobvalues[j].value);
                    dates.push(returnedData.results[i].jobvalues[j].job.time_start);
                }
            }

            this.compareValuesChild.title = "Trend plot for " + thisRow.name;
            this.compareValuesChild.data = values;
            this.compareValuesChild.labels = dates;
            this.compareValuesChild.plot();
        });
    };

    onPageSizeChange(event: {type: string, value: number}) {
        if (event.value) {
          this.pageSize = Number(event.value);
          // Update table
        }
    };
}
