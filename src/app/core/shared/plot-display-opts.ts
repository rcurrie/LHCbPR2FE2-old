export enum PRPlotDisplayOpts {
  split = 'Split',
  superimposed = 'Superimposed',
  ratio = 'Ratio',
  difference = 'Difference',
  kolmogorov = 'Kolmogorov'
}
