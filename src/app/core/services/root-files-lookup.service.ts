import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, Observer, forkJoin } from 'rxjs';

import { PRSearchJobsService } from './search-jobs.service';
import { PRSelectionStateService } from './selection-state.service';

import { PR_API_MAP } from '../../shared/api-map';
import { PRIRootFileLookupParams } from '../shared/root-file-lookup-params';
import { PRPlotDisplayOpts } from '../shared/plot-display-opts';
import { PRIRootFileLookupResponse } from '../shared/root-file-lookup-response';

import { PRIJobsComparisonParams } from '../../shared/interfaces/jobs-comparison-params';
import { PRJobsComparisonResponse } from '../../shared/classes/jobs-comparison-response';

import { PRTreeViewLeaf, PRTreeViewNode } from '../shared/tree-view-elements';

import { forEach, keys, without, has, unset, dropRight, clone } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class PRRootFilesLookupService {
  constructor(private _http: HttpClient,
              private _jobs: PRSearchJobsService,
              private _state: PRSelectionStateService) { }

  public getLookupDirs(params: PRIRootFileLookupParams): Observable<PRIRootFileLookupResponse> {
    console.log('DEBUG: getLookupDirs called with params: ' + JSON.stringify(params));
    return this._http.get<PRIRootFileLookupResponse>(
      `${PR_API_MAP.url_root}`,
      { params: <any>params }
    );
  }

  public getPlots(items: PRTreeViewLeaf[] = [],
                  compute: PRPlotDisplayOpts = PRPlotDisplayOpts.split): Observable<PRIRootFileLookupResponse[]> {
    console.log('DEBUG: getPlots called with params: ', items, compute);

    const parallel_reqs_params: PRIRootFileLookupParams[] = [];

    items.map((item) => {
      // Temp fix for the bug in ROOT service on BE
      if (!item.label.lastIndexOf('/')) {
        item.label = item.label.replace('/', '');
      }

      const params: PRIRootFileLookupParams = {
        files: item.jobfileids.join('__'),
        items: item.label
      };

      // Temp fix for the bug in ROOT service on BE
      if (compute !== PRPlotDisplayOpts.split && compute !== PRPlotDisplayOpts.superimposed) {
        params.compute = compute;
      }

      parallel_reqs_params.push(params);
    });

    const reqs_observables = parallel_reqs_params.map((params) => {
      return this.getLookupDirs(params);
    });

    return forkJoin(reqs_observables);
  }

  public getTreeView(ids: number[] = []): Observable<any> {
    // const tree_view: PRTreeViewNode[] = [];
    const tree_view = [];
    const jobs_data = {};

    return Observable.create((obs: Observer<any>) => {
      const compare_params: PRIJobsComparisonParams = {
       ids: ids.slice(),
       type: 'File'
      };

      // Get the data of the analysed jobs
      this._jobs.getCompareJobs(this._state.getCompareJobsHttpParams(compare_params))
          .subscribe(
            (cmp_res: PRJobsComparisonResponse) => {
              // const number_of_compared_jobs = ids.length;
              // let total_files_left = 0;

              // Map file types to jobs names
              forEach(cmp_res.results, (file_type) => {
                jobs_data[file_type.name] = { jobids: [] };
                forEach(file_type.jobvalues, (file) => {
                  if (file.value.endsWith('.root')) {
                    // total_files_left++;
                    jobs_data[file_type.name]['jobids'].push(file.job.id + '/' + file.value);
                  }
                });
              });

              forEach(jobs_data, (files, root_file_type) => {
                // Prepare structure to store full trees
                jobs_data[root_file_type]['full_trees'] = {};

                const root_file_trees = {};
                let files_left = files['jobids'].length;
                const params: PRIRootFileLookupParams = {
                  files: files['jobids'].join('__'),
                  folders: '/'
                };

                // Request the ROOT Files tree
                this.getLookupDirs(params)
                    .subscribe((res: PRIRootFileLookupResponse) => {
                      // Store ROOT files trees contents
                      forEach(res.result, (root_file) => {
                        files_left--;
                        // total_files_left--;
                        forEach(root_file.trees, (tree) => {
                          root_file_trees[root_file.root] = root_file.trees;
                          jobs_data[root_file_type]['full_trees'][root_file.root] = root_file.trees;
                        });
                        // If all files have been processed
                        if (files_left === 0) {
                          jobs_data[root_file_type]['intersection_tree'] =
                            this._rootFilesTreesIntersection(clone(jobs_data[root_file_type]['full_trees'][root_file.root]),
                            root_file_type,
                            jobs_data[root_file_type]['full_trees']);

                          // Create structure for TreeView
                          tree_view.push(new PRTreeViewNode(root_file_type,
                                                            false,
                                                            true,
                                                            this._createTreeViewStructure(
                                                              jobs_data[root_file_type]['intersection_tree'],
                                                              root_file_type,
                                                              files['jobids'].slice())
                                                            ));
                        }
                      });

                      // After processing all trees
                      // if (total_files_left === 0) {
                        // console.log('DEBUG: TreeStruct:\n', tree_view);
                        // console.log('DEBUG: JobsData:\n', jobs_data);
                      // }
                    });
              });

      // Return the TreeView to the component
      obs.next(tree_view);
          },
          (error) => { obs.error(error); },
          () => { obs.complete(); });
    });
  }

  // Method which makes a deep intersection of objects,
  // only the common properties are left in the end
  private _rootFilesTreesIntersection(obj: {}, file_type = null, full_trees = [], path = []) {
    const file_names = keys(full_trees);
    forEach(obj, (val, key) => {
      path.push(key);
      for (let i = 1; i < file_names.length; i++) {
        if (!has(full_trees[file_names[i]], path)) {
          unset(obj, key);
          without(path, key);
        }
      }

      if (typeof(val) === 'object') {
        this._rootFilesTreesIntersection(val, file_type, full_trees, path);
        path = dropRight(path, 2);
      } else {
        path = dropRight(path);
      }

      if (path.length === 1 && path[0] === key) {
        path = dropRight(path);
      }
    });

    return(obj);
  }

  // Transform the intersected datastructure into
  // one suitable for Tree View
  private _createTreeViewStructure(obj: {}, filetype = '', jobfileids = [], parent: PRTreeViewNode = null, tree_view = []) {
    forEach(obj, (val, key) => {
      // Is this a node with leaves?
      if (typeof(val) === 'object') {
        const new_node = new PRTreeViewNode(key);

        // Descend further into the node
        this._createTreeViewStructure(val, filetype, jobfileids, new_node, tree_view);

        if (parent) {
          parent.children.push(new_node);
        } else {
          tree_view.push(new_node);
        }
      } else { // This is a leaf
        const new_node = new PRTreeViewLeaf(key, false, filetype, jobfileids);
        parent.children.push(new_node);
      }
    });

    if (!parent) {
      return(tree_view);
    }
  }
}
