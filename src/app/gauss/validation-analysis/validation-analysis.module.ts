import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClarityModule } from '@clr/angular';

import { PRValidationAnalysisRoutingModule } from './validation-analysis.routing.module';
import { PRSharedModule } from '../../shared/shared.module';
import { PRROOTFileViewerModule } from '../../ROOT-file-viewer/ROOT-file-viewer.module';

import { PRValidationAnalysisComponent } from './validation-analysis.component';

@NgModule({
  imports: [
    CommonModule,
    ClarityModule,
    PRValidationAnalysisRoutingModule,
    PRSharedModule,
    PRROOTFileViewerModule
  ],
  declarations: [
   PRValidationAnalysisComponent
  ],
  exports: [
    PRValidationAnalysisComponent
  ]
})
export class PRValidationAnalysisModule { }
