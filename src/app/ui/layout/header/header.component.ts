import { Component } from '@angular/core';

import { NAVIGATION_ITEMS } from '../../../shared/navitems';

import { Router, RouterEvent, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class PRHeaderComponent {
  activeNavigationItemId: string = 'jobs';
  navigationItems = NAVIGATION_ITEMS;

  constructor(private router: Router) {
    router.events.subscribe(e => {
      if(!(e instanceof NavigationEnd)) {
        return;
      }
      for(var i = 0; i < this.navigationItems.length; i++) {
        var thisItem = this.navigationItems[i];
        if(router.isActive(thisItem.routerLink, thisItem.activeExact)) {
          this.setActiveNavigationItemId(thisItem.id);
        }
      }
    });
  }

  setActiveNavigationItemId(id: string) {
    this.activeNavigationItemId = id;
  }

}
