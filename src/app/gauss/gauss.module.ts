import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PRGaussRoutingModule } from './gauss.routing.module';
import { PRSharedModule } from '../shared/shared.module';

import { PRGaussComponent } from './gauss.component';

@NgModule({
  imports: [
    CommonModule,
    PRGaussRoutingModule,
    PRSharedModule
  ],
  declarations: [
    PRGaussComponent
  ]
})
export class PRGaussModule { }
