
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SortOrder } from '@clr/angular';

import { JSONFormatRule } from './json-format.component';
import { JSONQueryService } from '../../core/services/json-query/json-query.service';
import { HLTCompareValuesComponent } from '../compare-values/compare-values.component';
import { SingleJobSelectorState } from '../../core/services/json-query/single-job-selector-state';

@Component({
  selector: 'app-json-table',
  templateUrl: './json-table.component.html'
})
export class JSONTableService implements OnInit {

  @ViewChild(HLTCompareValuesComponent) compareValuesChild: HLTCompareValuesComponent;
  @ViewChild('thisDataGrid') thisDataGrid: any;

  viewableRows: object[] = [];
  viewableCols: object[] = [];
  columns = {};
  formattingRules: JSONFormatRule;
  pageSizes: number[] = [10,25,50,100];
  pageSize: number = 10;
  colPrecisionCache: object = {};
  loading: boolean = false;
  descSort = SortOrder.Desc;
  selectorState: SingleJobSelectorState=new SingleJobSelectorState();
  private _inputJob: number;
  private _appName: string;
  private _filterArr: Array<string[]>=[[],[]];
  get inputJob(): number {
    return this._inputJob;
  }
  @Input()
  set inputJob(inputJob: number) {
    this._inputJob = inputJob;
    if(!this._inputJob) {
      this.viewableRows = [];
    }
  }
  get appName(): string {
    return this._appName;
  }
  @Input()
  set appName(appName: string) {
    this._appName = appName;
    this.queryService.appName = this._appName;
  }

  @Input()
  set filter(filterArr: Array<string[]>) {
    this._filterArr = filterArr;
    if(this._filterArr.length) {
      this.renderTable();
    }
    else {
      this.viewableRows = [];
    }

  };

  constructor(private queryService: JSONQueryService) {
  };

  setupTable(formatRules: JSONFormatRule) {
    this.formattingRules = formatRules;
    this.viewableCols = this.formattingRules.getColObjs();
  };

  clearTable() {
    this.viewableCols = [];
    this.filter = [];
    if(this.formattingRules) this.formattingRules.stripName = '';
  };

  private renderTable() {
    this.loading = true;
    this.queryService.getAllJSONResults(this._inputJob, this._filterArr[0], this._filterArr[1]).subscribe(
      JSONData => {
       this.formatData(JSONData.results[0].value);
       this.loading = false;
     });
  };

  /*
  setupQueries(query_service: JSONQueryService) {
    this.queryService = query_service;
  }*/

  ngOnInit() {
  };

  displayData(displayData: object[]) {
    this.viewableRows = displayData;
  };

  formatData(this_data: object) {
    var formattedData = [];
    for(var name in this_data) {
      var rowObject = this.formatRow(name, this_data[name]);
      formattedData.push(rowObject);
    }
    this.viewableRows = formattedData;
  };

  colPrecision(this_col: string): number {
    var precision = 1;
    if( this_col in this.colPrecisionCache ) {
      return this.colPrecisionCache[this_col];
    }

    var this_precision = this.formattingRules.getPrecision(this_col);
    precision = this_precision > 0 ? this_precision : precision;

    this.colPrecisionCache[this_col] = precision;
    return precision;
  }

  formatRow(name: string, this_row: object): object {
    var thisRow = {'name': this.formattingRules.getRowName(name)};
    thisRow['real_name'] = name;

    for( var this_obj in this_row ) {
      var objName = String(this_obj);
      var precision = this.colPrecision(objName);

//      console.log(objName + ' = ' + this_row[this_obj]['value']);

      if(this_row[this_obj] instanceof Object) {
        var thisObj = this_row[this_obj];
        if('average' in thisObj) {
          var stringVal = String(thisObj['average']);
          if('error' in thisObj) {
            stringVal = parseFloat(stringVal).toFixed(precision) + " ± " + parseFloat(thisObj['error']).toFixed(precision);
          }
          thisRow[objName+'_str'] = stringVal;
          thisRow[objName+'_value'] = thisObj['average'];
          continue;
        }
      }
      var stringVal = String(this_row[this_obj]);

      var errName =  objName+"_err";
      if( errName in this_row ) {
        stringVal = parseFloat(stringVal).toFixed(precision) + " ± " + parseFloat(this_row[errName]).toFixed(precision);
        thisRow[objName+'_str'] = stringVal;
        continue;
      }

      if( !isNaN(parseFloat(this_row[this_obj])) ) {
        stringVal = parseFloat(stringVal).toFixed(precision);
      }

      thisRow[objName+'_str'] = stringVal;
    }

    return thisRow;
  };

  onPageSizeChange(event: {type: string, value: number}) {
    if (event.value) {
      this.pageSize = Number(event.value);
      // Update table
    }
  };

  makeTrend(col: object, row: object) {
    if( col['key'].endsWith('_str') ) {
      var colName = col['key'].substring(0, col['key'].indexOf('_str'))
    } else {
      var colName = col['key'];
    }
    var rowName = row['real_name'];

    console.log("Trend for: "+ colName + " & " + rowName);

    var app_version = this.selectorState['build_id'] ? this.selectorState['build_id'] : '';
    this.queryService.getJSONTrendData(colName, rowName, app_version, 21, []).subscribe( JSONTrendData => {
      var plottableData = [];
      var plottableError = [];
      var plottableLabels = [];
      for( const data_i in JSONTrendData['results'] ) {
        var data_t = JSONTrendData['results'][data_i];
        var value = data_t[colName];
        if(value instanceof Object) {
          plottableData.push(value['average']);
          plottableError.push(value['error']);
        } else {
          plottableData.push(data_t[colName]);
          plottableError.push(data_t[colName+'_err']);
        }
        plottableLabels.push(data_t['date']);
      }
      this.compareValuesChild.title = "Trend for: "+ colName + " & " + rowName;
      this.compareValuesChild.data = plottableData;
      this.compareValuesChild.error = plottableError;
      this.compareValuesChild.labels = plottableLabels;
      this.compareValuesChild.plot();
    });
    return;
  };

  findColKeyByName(colName: string): string {
    var wanted_col = '';
    var colRules = this.formattingRules.getColObjs();
    for( var i = 0; i< colRules.length; i++ ) {
      if( colRules[i]['name'] == colName ) {
        wanted_col = colRules[i]['key'];
      }
    }
    return wanted_col;
  }

};
