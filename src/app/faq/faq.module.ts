import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PRFAQRoutingModule } from './faq.routing.module';
import { PRSharedModule } from '../shared/shared.module';

import { PRFAQComponent } from './faq.component';

@NgModule({
  imports: [
    CommonModule,
    PRFAQRoutingModule,
    PRSharedModule
  ],
  declarations: [
    PRFAQComponent
  ]
})
export class PRFAQModule { }
