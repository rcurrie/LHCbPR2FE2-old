import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PRFAQComponent } from './faq.component';

export const routes: Routes = [
  { path: 'faq', component: PRFAQComponent },
  { path: '', pathMatch: 'full', redirectTo: 'faq' },
  { path: '**', redirectTo: 'faq' },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PRFAQRoutingModule { }
