
export class SingleJobSelectorState {
    build_id: string;
    option_id: string;
    platform_id: string;
    job_id: number;
    app_id: string;
    exec_id: string;

    resetBuild() {
        this.resetOption();
        this.option_id = "";
    };

    resetOption() {
        this.resetPlatform();
        this.platform_id = "";
    };

    resetPlatform() {
        this.job_id = 0;
    };

    resetAll() {
        this.resetBuild();
        this.build_id = "";
    };

    toQueryParms() {
        var output = {
            'jid': this.job_id
        }
        if(this.job_id == 0) {
            delete output.jid;
        }
        return output;
    };

};
