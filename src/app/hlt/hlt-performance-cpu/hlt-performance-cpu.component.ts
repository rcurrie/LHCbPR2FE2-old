import { Component, OnInit, ViewChild } from '@angular/core';

import { JSONQueryService } from '../../core/services/json-query/json-query.service';
import { PlotlyChartData } from '../../shared/components/plotly-chart/plotly-chart.component';
import { Subscriber } from 'rxjs';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { simpleCol, SimpleTableService } from '../simple-table/simple-table.component';
import { resultValues, rawResultData } from '../../core/services/json-query/json-query-interface';
import { SingleJobSelectorComponent } from '../../shared/components/single-job-selector/single-job-selector.component';

@Component({
  selector: 'app-cpu-performance',
  templateUrl: './hlt-performance-cpu.component.html'
})
export class PRHLTCPUPerformance implements OnInit {

    perfData: resultValues[];
    rawperfData: rawResultData;
    parents: object = {};
    children: object[] = [];
    hasDataLoaded: boolean = false;
    progress: number = 0;
    svgPath: SafeResourceUrl;
    jobId: number = 0;
    appName: string = 'MOORE';
    execName: string = 'perf-lb-run';

    @ViewChild('chart1') _chart1: PlotlyChartData;


    tableCols: simpleCol[];
    tableData: object[];
    @ViewChild('table') _table: SimpleTableService;

    @ViewChild('jobSelector') _selector: SingleJobSelectorComponent;

    constructor( private _queryService: JSONQueryService, private sanitizer: DomSanitizer ) {
    };

    /**
    * Returns the list of names of the top `wanted_entries` of parameters which have the largest `data_str` entry in `names_data`
    *
    */
    getTopSortedNames(names_data: object, data_str: string, wanted_entries: number, cutoff: number): string[] {
        var unsorted_data = [];

        for(var name in names_data) {
            const this_obj = {'name': name, 'value': names_data[name][data_str]};
            unsorted_data.push(this_obj);
        }

        var sorted_data = unsorted_data.sort( (a,b) => {
            return a['value'] - b['value'];
        });

        sorted_data = sorted_data.reverse();
        sorted_data = sorted_data.slice(0, wanted_entries);

        var sorted_names = [];
        for(var i = 0; i< sorted_data.length; i++) {
            if(sorted_data[i]['value'] > cutoff)
                sorted_names.push(sorted_data[i]['name']);
        }

        return sorted_names;
    };

    getTopSortedData(names_data: object, display_list: string[], data_str: string): number[] {
        var returnable=[];

        for(var i=0; i< display_list.length; i++) {
            returnable.push(names_data[display_list[i]][data_str]);
        }

        return returnable;
    };

    getOtherParams(names_data: object, data_str: string, display_list: string[], max: number): object[] {

        var other=0.;
        var total=0.;

        for(var name in names_data) {
            total = total + names_data[name][data_str];
            if(!(display_list.includes(name))) {
                other = other + names_data[name][data_str];
            }
        }

        var returnable = [{name: 'Other Methods', value: other}];
        if( (max-total) > 1E-2 ) {
            returnable.push({name: 'Unknown %-age', value: max-total});
        }
        return returnable;
    };

    getProgressSubscriber() : Subscriber<number> {
        return new Subscriber((progress) => {
            console.log("progress change");
            this.progress = progress;
        });
    };

    getSVGPath(job_id: number): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl('https://lblhcbpr.cern.ch/api/media/jobs/'+String(job_id)+'/perf.svg');
    };

    ngOnInit() {
    };

    getCorrectonFactor(job_id): number {
        var output: any;
        this._queryService.getRawData(String(job_id)).subscribe( returnedData => {
            output = this.calcCorrectionFactor(returnedData.results[0].results.Float);
        });
        return output;
    };

    calcCorrectionFactor(inputData: object[], max: number=100): number {
        var total=0.;
        for( var i=0; i< inputData.length; i++ ) {
            if( String(inputData[i]['name']).endsWith('_self') ) {
                total=total+parseFloat(inputData[i]['value']);
            }
        }
        return total/max;
    };

    performQuery(job_id: number) {

        this._queryService.getRawData(String(job_id)).subscribe( returnedData => {
            console.log("HERE IS THE RAW DATA!");

            var newVar = this.getCorrectonFactor(String(job_id));

            var factor = this.calcCorrectionFactor(returnedData.results[0].results.Float);

            console.log("Correction Factor: " + factor);
            console.log("newVar " + newVar);

            var newdict = {};

            this.rawperfData = returnedData.results[0].results;
            for( var i=0; i< this.rawperfData.Float.length; i++ ) {
                var thisObj = this.rawperfData.Float[i];
                var returnedValue = thisObj.value;
                var returnedName = thisObj.name;
                var nameLength = returnedName.length - returnedName.lastIndexOf('_');
                var dict_name = returnedName.slice(0,-nameLength);
                var this_name = returnedName.slice(returnedName.lastIndexOf('_')+1,returnedName.length);;
                if( !(dict_name in newdict) ) {
                    newdict[dict_name] = {};
                }
                newdict[dict_name][this_name]=parseFloat(returnedValue)/factor;
            }

            var top_labels = this.getTopSortedNames(newdict, 'self', 15, 0.0);
            var top_data = this.getTopSortedData(newdict, top_labels, 'self');
            var other_data = this.getOtherParams(newdict, 'self', top_labels, 100.);

            console.log(top_labels);

            var top_pulls=[];
            for(var i=0; i< top_labels.length; i++) {
                top_pulls.push(0.3);
            }
            for(var i=0; i< other_data.length; i++) {
                top_labels.push(other_data[i]['name']);
                top_data.push(other_data[i]['value']);
                top_pulls.push(0.);
            }

            this._chart1.labels = top_labels.slice();
            this._chart1.data = top_data.slice();
            this._chart1.pulls = top_pulls.slice();
            this._chart1.type = "pie";
            this._chart1.registerClickHandler(this.clickyHere);
            this._chart1.plot();

            this.hasDataLoaded = true;

            this.svgPath = this.getSVGPath(job_id);
        });

        /*
        this._queryService.getAllPerfData(String(job_id), this.getProgressSubscriber()).subscribe( returnedData => {

            if(returnedData.results.length != returnedData.count) console.log("ERROR IN REQUESTING DATA!");

            this.perfData = returnedData.results;
            var total=0;
            for( var i=0; i< this.perfData.length; i++) {
                if( String(this.perfData[i]['name']).endsWith('_self') ) {
                    this.parents[this.perfData[i]['name'].slice(0,-5)] =
                        {'self_time': parseFloat(this.perfData[i].jobvalues[0].value),
                         'self_id': this.perfData[i].id };
                        total = total+parseFloat(this.perfData[i]['jobvalues'][0]['value']);
                }
            }
            console.log("Original Total: " + total);
            console.log("PerfLen: " + this.perfData.length);
            for( var k in this.parents ) {
                const childName = k+'_children';
                for( var j=0; j< this.perfData.length; j++ ) {
                    if( childName == this.perfData[j]['name'] ) {
                        this.parents[k]['child_time'] = parseFloat(this.perfData[j].jobvalues[0].value);
                        this.parents[k]['child_id'] = this.perfData[j].id;
                        break;
                    }
                }
            }
            console.log("PARENTS");
            console.log(this.parents);

            var top_labels = this.getTopSortedNames(this.parents, 'self_time', 15, 0.0);
            var top_data = this.getTopSortedData(this.parents, top_labels, 'self_time');
            var other_data = this.getOtherParams(this.parents, 'self_time', top_labels, 100.);


            var top_pulls=[];
            for(var i=0; i< top_labels.length; i++) {
                top_pulls.push(0.3);
            }
            for(var i=0; i< other_data.length; i++) {
                top_labels.push(other_data[i]['name']);
                top_data.push(other_data[i]['value']);
                top_pulls.push(0.);
            }

            this._chart1.labels = top_labels.slice();
            this._chart1.data = top_data.slice();
            this._chart1.pulls = top_pulls.slice();
            this._chart1.type = "pie";
            this._chart1.registerClickHandler(this.clickyHere);
            this._chart1.plot();

            this.hasDataLoaded = true;

            this.svgPath = this.getSVGPath(job_id);




            var nameCol = new simpleCol();
            nameCol.name = 'Name';
            nameCol.key = 'name';
            var selfCol = new simpleCol();
            selfCol.name = 'Time spent in function (%)';
            selfCol.key = 'self';
            selfCol.trendable = true;
            selfCol.trend_key = 'self_id';
            var childCol = new simpleCol();
            childCol.name = 'Time spent by children (%)';
            childCol.key = 'children';
            childCol.trendable = true;
            childCol.trend_key = 'child_id';

            this.tableCols = [];
            this.tableCols.push(nameCol);
            this.tableCols.push(selfCol);
            this.tableCols.push(childCol);

            this.tableData = [];
            for( var name in this.parents ) {
                var thisRow = {'name': name,
                               'self': this.parents[name]['self_time'],
                               'children': this.parents[name]['child_time'],
                               'self_id': this.parents[name]['self_id'],
                               'child_id': this.parents[name]['child_id']};
                this.tableData.push(thisRow);
            }

            this._table.defineCols(this.tableCols);
            this._table.sendData(this.tableData);
            this._table.setComparisonJobs(this._selector.possibleJobs);
        });

        */
    };

    clickyHere(data) {
        console.log("Selected: " + data.points[0].label);
    };

    jobChanged(newJobID: number) {
        this.jobId = newJobID;
        if(newJobID) {
            this.progress = 0;
            this.hasDataLoaded = false;
            this.svgPath = undefined;
            this._table.clearTable();
            this.performQuery(newJobID);
        }
    };

}
