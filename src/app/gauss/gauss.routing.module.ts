import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PRGaussComponent } from './gauss.component';

export const routes: Routes = [
  { path: 'validan', loadChildren: './validation-analysis/validation-analysis.module#PRValidationAnalysisModule' },
  { path: 'numan', component: PRGaussComponent, pathMatch: 'full' },
  { path: 'hadrxs', component: PRGaussComponent, pathMatch: 'full' },
  { path: 'detime', loadChildren: './detailed-timing/detailed-timing.module#PRDetailedTimingModule' },
  { path: '', pathMatch: 'full', redirectTo: 'validan' },
  { path: '**', redirectTo: 'validan' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PRGaussRoutingModule { }
