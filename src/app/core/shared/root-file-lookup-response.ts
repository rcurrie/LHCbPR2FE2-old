export interface PRIRootFileLookupResponse {
  result: [
    {
      items?: {},
      root?: string,
      trees?: {},
      computed_result?: {},
      KSTest: number
    }
  ];
}
