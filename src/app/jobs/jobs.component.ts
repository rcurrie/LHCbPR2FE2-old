import { Component, ViewChild } from '@angular/core';

import { PRISelectionEvent } from '../shared/interfaces/selection-event';
import { PRSelectJobsComponent } from '../search-jobs/select-jobs/select-jobs.component';
import { PRSelectValuesComponent } from './select-values/select-values.component';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html'
})
export class PRJobsComponent {
  @ViewChild(PRSelectJobsComponent) selectJobsChild: PRSelectJobsComponent;
  @ViewChild(PRSelectValuesComponent) selectValuesChild: PRSelectValuesComponent;
  private _jobsIDs: number[] = [];

  // Triggered by: SearchJobs when selection criteria change
  // Triggers: SelectJobs to refresh the list of jobs
  onSearchSelectionChanged() {
    this.selectJobsChild.selectedItems = [];

    if (this.selectValuesChild.visible) {
      this.selectValuesChild.visible = false;
      this.selectJobsChild.visible = true;
    }

    this.selectJobsChild.refreshDatagrid();
  }

  // Triggered by: SelectJobs when 'Analyse' button is clicked
  // Triggers: SelectJobs and SelectValues
  onAnalyseJobs(payload?: PRISelectionEvent) {
    this.selectJobsChild.visible = false;
    this.selectValuesChild.jobsIDs = payload.data.slice();
    this.selectValuesChild.visible = true;
    this.selectValuesChild.onRefreshDatagrid();
  }

  // Triggerd by: SelectValues when 'return to search' button is clicked
  // Triggers: SelectJobs and SelectValues
  onExitComparingJobs($event) {
    this.selectValuesChild.visible = false;
    this.selectJobsChild.visible = true;
  }
}
