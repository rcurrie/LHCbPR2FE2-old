import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'breakLongString'})
export class PRBreakLongStringPipe implements PipeTransform {
  transform(value: string, breakBy: string) {
    return value.replace(new RegExp(this._escapeRegExp(breakBy), 'g'), ` ${breakBy} `);
  }

  private _escapeRegExp(value: string) {
    return value.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
  }
}
