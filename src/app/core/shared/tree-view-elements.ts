export class PRTreeViewLeaf {
  label: string;
  selected = false;
  filetype: string;
  jobfileids: any[];

  constructor(label: string, selected = false, filetype = '', jobfileids = []) {
    this.label = label;
    this.selected = selected;
    this.filetype = filetype;
    this.jobfileids = jobfileids;
  }
}

export class PRTreeViewNode {
  label: string;
  selected = false;
  expanded = true;
  children: any[];

  constructor(label: string, selected = false, expanded = true, children = []) {
    this.label = label;
    this.selected = selected;
    this.expanded = expanded;
    this.children = children;
  }
}
