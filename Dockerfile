# Stage 1: build
FROM node:9-alpine as app-builder

ARG APP_SRC_DIR=/appsrc

RUN apk update \
    && apk add --no-cache nano git curl bash \
    && npm set progress=false \
    && npm config set depth 0 \
    && mkdir $APP_SRC_DIR

WORKDIR $APP_SRC_DIR

COPY ./package.json ./

# Store Node modules on a separate layer to prevent installing at each build
RUN npm install

COPY . .

# Build the application in prod mode
RUN $(npm bin)/ng build --prod --build-optimizer

############################################################
# Stage 2: setup & serve
FROM nginx:mainline-alpine

ARG APP_SRC_DIR=/appsrc

COPY --from=app-builder $APP_SRC_DIR/nginx/default.conf /etc/nginx/conf.d/

RUN rm -rf /usr/share/nginx/html/*

# Copy artifacts from the build stage
COPY --from=app-builder $APP_SRC_DIR/dist /usr/share/nginx/html

ENTRYPOINT ["nginx", "-g", "daemon off;"]
