import { PRJob } from './job';

export class PRJobsDataResponse {
  'count': number;
  'next': string;
  'previous': string;
  'results': PRJob[];
}
