import { Component, Input, Output, EventEmitter } from '@angular/core';

import { PRIBasicApiResponse } from '../../shared/interfaces/basic-api-response';
import { PRISelectionEvent } from '../../shared/interfaces/selection-event';

@Component({
  selector: 'app-basic-filter',
  template: `
<ng-template [ngIf]="items.length">
  <div class="row">
    <div class="col-xs-12">
      <h5>{{ label }}</h5>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-11 push-xs-1">
      <clr-checkbox *ngFor="let item of items"
                    (clrCheckedChange)="onSelectionChange(item.id, $event)"
                    [clrChecked]="filteredItems.includes(item.id)">
        {{ item.name }} ({{ item.count }})
      </clr-checkbox>
    </div>
  </div>
</ng-template>
`
})
export class PRBasicFilterComponent {
  @Input() label = 'NONAME';
  @Input() items: PRIBasicApiResponse[] = [];
  @Input() filteredItems: number[] = [];
  @Output() selectionChanged: EventEmitter<PRISelectionEvent> = new EventEmitter<PRISelectionEvent>();

  onSelectionChange(id: number, isSelected: boolean) {
    if (isSelected && !this.filteredItems.includes(id)) {
      this.filteredItems.push(id);
      this._emittEvent();
    } else if (!isSelected) {
      const index = this.filteredItems.indexOf(id, 0);
      if (index !== -1) {
        this.filteredItems.splice(index, 1);
        this._emittEvent();
      }
    }
  }

  private _emittEvent() {
    this.selectionChanged.emit({
      type: this.label.toLowerCase(),
      data: this.filteredItems.slice()
    });
  }
}
