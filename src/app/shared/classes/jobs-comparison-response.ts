import { PRMeasure } from './measure';

export class PRJobsComparisonResponse {
  'count': number;
  'next': string;
  'previous': string;
  'results': PRMeasure[];
}
