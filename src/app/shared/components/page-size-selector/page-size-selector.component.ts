import { Component, Input, Output, EventEmitter } from '@angular/core';

import { PRSelectionState } from '../../classes/selection-state';

@Component({
  selector: 'app-page-size-selector',
  template: `Results per page:&nbsp;
<select style="margin-right: 11px" (change)="onPageSizeChange($event.target.value)">
  <option *ngFor="let item of pageSizes" value="{{ item }}">{{ item }}</option>
</select>
`
})
export class PRPageSizeSelectorComponent {
  @Input() pageSizes: number[];
  @Output() newPageSize: EventEmitter<any> = new EventEmitter<any>();
  private _size: number;

  constructor() {
    this._size = this.pageSizes ? this.pageSizes[0] : 10;
  }

  onPageSizeChange(size: number) {
    this._size = size;
    this.newPageSize.emit({ type: 'pagesize', value: this._size });
  }
}
