import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

import { PRSelectionState } from './../shared/classes/selection-state';
import { PRSearchJobsService } from '../core/services/search-jobs.service';
import { PRSelectionStateService } from '../core/services/selection-state.service';
import { PRBreakLongStringPipe } from '../shared/pipes/break-long-string.pipe';

import { PRIBasicApiResponse } from '../shared/interfaces/basic-api-response';
import { PRIVersionsApiResponse } from '../shared/interfaces/versions-api-response';
import { PRJobFiltersPresets } from '../shared/classes/job-filters-presets';
import { PRISelectionEvent } from './../shared/interfaces/selection-event';

import { PRBasicFilterComponent } from './filter-components/basic-filter.component';
import { PRVesionsFilterComponent } from './filter-components/versions-filter.component';

@Component({
  selector: 'app-search-jobs',
  templateUrl: './search-jobs.component.html'
})
export class PRSearchJobsComponent implements OnInit {
  @Output() selectionChangedEvt: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() filtersPresets: PRJobFiltersPresets = new PRJobFiltersPresets();
  applications: PRIBasicApiResponse[] = [];
  jobsSearchForm: FormGroup;
  selectionState = new PRSelectionState();

  constructor(private _jobs: PRSearchJobsService, private _fb: FormBuilder, private _state: PRSelectionStateService) { }

  ngOnInit() {
    // Create form elements: application selector, nightlies button, number of nightlies to show
    this.jobsSearchForm = this._fb.group({
      'app_id': '0',
      'toggle_show_versions': false,
      'number_of_versions': '10'
    });

    // Get the available active applications (Gauss, Geant4, Brunel, etc.)
    this._jobs.getApplications()
      .subscribe(apps => {
        this.applications = apps;
      });

    // Monitor when a new application is selected
    this.jobsSearchForm.controls['app_id'].valueChanges
      .subscribe((value) => {
        // Reset the state
        this.selectionState.resetAll();
        this._state.resetState();

        // Set the new selected application id
        this.selectionState.app_id = Number(this.jobsSearchForm.controls['app_id'].value);
        this._state.setAppID(Number(this.jobsSearchForm.controls['app_id'].value));

        // Use filter presets
        if (this.filtersPresets.options_ids.length) {
          this.selectionState.filtered_options = this.filtersPresets.options_ids.slice();
          this._state.setFilteredOptions(this.filtersPresets.options_ids.slice());
        }

        this._onAppChange();
      });

    // Monitor when versions should be displayed/hidden
    this.jobsSearchForm.controls['toggle_show_versions'].valueChanges
      .subscribe((value) => {
        this.selectionState.with_nightly = value;
        this.selectionState.check_specific = value;
        this._state.setWithNightly(value);
        this._state.setCheckSpecific(value);
        if (value) {
          this._updateVersions();
        }
      });

    // Monitor when the number of versions for the applications changes
    // to display more or less
    this.jobsSearchForm.controls['number_of_versions'].valueChanges
      .subscribe((value) => {
        this.selectionState.nightly_version_number = Number(value);
        this._state.setNightlyVersionNumber(Number(value));
        this._updateVersions();
      });

    this.jobsSearchForm.controls['app_id'].setValue(this.filtersPresets.app_id);
  }

  // Method triggered when a new application is selected
  // Get all the data for the new application
  private _onAppChange() {
    // Do nothing if no application is selected
    if (!this.selectionState.app_id) {
      return;
    }

    // Fetch versions, etc. for the application
    this._updateVersions();

    this._jobs.getOptionsForApplicaton(this.selectionState.app_id)
      .subscribe(res => {
        if (this.filtersPresets.options_ids.length) {
          // Example with lodash's keyBy
          // _(res)
          // .keyBy('id')
          // .at(this.filtersPresets.options_ids)
          // .value();

          const options = res
            .slice()
            .filter(function(e) { return this.indexOf(e.id) >= 0; }, this.filtersPresets.options_ids);

          this.selectionState.unfiltered_options = options.slice();
          this._state.setUnfilteredOptions(options.slice());

          this.selectionState.filtered_options = this.filtersPresets.options_ids.slice();
          this._state.setFilteredOptions(this.filtersPresets.options_ids.slice());
        } else {
          this.selectionState.unfiltered_options = res;
          this._state.setUnfilteredOptions(res.slice());
        }
      });

    this._jobs.getExecutablesForApplicaton(this.selectionState.app_id)
      .subscribe(res => {
        this.selectionState.unfiltered_executables = res;
        this._state.setUnfilteredExecutables(res.slice());
      });

    this._jobs.getPlatformsForApplicaton(this.selectionState.app_id)
      .subscribe(res => {
        this.selectionState.unfiltered_platforms = res;
        this._state.setUnfilteredPlatforms(res.slice());
      });

    this._jobs.getHostsForApplicaton(this.selectionState.app_id)
      .subscribe(res => {
        this.selectionState.unfiltered_hosts = res;
        this._state.setUnfilteredHosts(res.slice());
      });

    this.onSelectionChange();
  }

  // Method returning HTTP parameters for fetching Versions from the back-end
  private _getHttpParamsForVersionsRequests(): {} {
  return ({
      'withNightly': this.jobsSearchForm.controls['toggle_show_versions'].value ? 'true' : 'false',
      'nightlyVersionNumber': String(this.jobsSearchForm.controls['number_of_versions'].value),
      'checkSpecific': this.selectionState.check_specific ? 'true' : 'false'
    });
  }

  // Method to fetch Versions for the active application via the SearchJobs service
  private _updateVersions() {
    this.selectionState.check_specific = this.selectionState.with_nightly;
    this._jobs.getVersionsForApplicaton(this.selectionState.app_id, this._getHttpParamsForVersionsRequests())
      .subscribe(res => {
        this.selectionState.unfiltered_versions = res;
        this._state.setUnfilteredVersions(res.slice());
      });
  }

  // Method triggered by any change in the selection (versions/options/etc.)
  // Constructs HTTP parameters for the request to the back-end
  // and fetches results via the SearchJobs service
  onSelectionChange(payload?: PRISelectionEvent) {
    if (payload) {
      if (payload.type === 'options') {
        this.selectionState.filtered_options = payload.data;
        this._state.setFilteredOptions(payload.data);
      } else if (payload.type === 'versions') {
        this.selectionState.filtered_versions = payload.data;
        this._state.setFilteredVersions(payload.data);
      } else if (payload.type === 'executables') {
        this.selectionState.filtered_executables = payload.data;
        this._state.setFilteredExecutables(payload.data);
      } else if (payload.type === 'platforms') {
        this.selectionState.filtered_platforms = payload.data;
        this._state.setFilteredPlatforms(payload.data);
      } else if (payload.type === 'hosts') {
        this.selectionState.filtered_hosts = payload.data;
        this._state.setFilteredHosts(payload.data);
      }
    }

    // Emitt event (to the parent component) that the selection criteria have changed
    this.selectionChangedEvt.emit(true);
  }
}
