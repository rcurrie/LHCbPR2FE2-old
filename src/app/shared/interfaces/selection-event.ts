export interface PRISelectionEvent {
  type: string;
  data: number[];
}
