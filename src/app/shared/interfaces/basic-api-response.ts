export interface PRIBasicApiResponse {
   id: number;
   name: string;
   count: number;
}
