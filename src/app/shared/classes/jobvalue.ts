import { PRJob } from './job';

export class PRJobValue {
  'job': PRJob;
  'value': string;
}
