import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ClarityModule } from '@clr/angular';

import { PRLayoutComponent } from './layout/layout.component';
import { PRHeaderComponent } from './layout/header/header.component';
import { PRMainComponent } from './layout/main/main.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ClarityModule
  ],
  declarations: [
    PRLayoutComponent,
    PRHeaderComponent,
    PRMainComponent
  ],
  exports: [
    PRLayoutComponent
  ]
})
export class PRUIModule { }
