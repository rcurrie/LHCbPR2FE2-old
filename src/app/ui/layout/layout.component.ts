import { Component } from '@angular/core';

@Component({
  selector: 'app-layout',
  template: `
    <div class="main-container" style="display:block; overflow:auto;">
      <app-header></app-header>
      <app-main>
        <ng-content></ng-content>
      </app-main>
    </div>
  `
})
export class PRLayoutComponent { }
