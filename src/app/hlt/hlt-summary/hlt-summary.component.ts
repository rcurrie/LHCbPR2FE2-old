import { Component, OnInit, ViewChild } from '@angular/core';

import { PlotlyChartData } from '../../shared/components/plotly-chart/plotly-chart.component';
import { JSONQueryService } from '../../core/services/json-query/json-query.service';
import { JobId } from '../../core/services/json-query/json-query-interface'
import { Router, Params } from '@angular/router';

@Component({
  selector: 'app-hlt',
  templateUrl: './hlt-summary.component.html',
  providers: [],
})
export class PRHLTSummaryComponent implements OnInit {

  users = [];
  value = {};
  value2 = {};
  testParam: string;
  @ViewChild('chart1') _chart1: PlotlyChartData;
  @ViewChild('chart2') _chart2: PlotlyChartData;
  @ViewChild('chart3') _chart3: PlotlyChartData;
  @ViewChild('chart4') _chart4: PlotlyChartData;
  @ViewChild('chart5') _chart5: PlotlyChartData;
  @ViewChild('chart6') _chart6: PlotlyChartData;
  @ViewChild('chart7') _chart7: PlotlyChartData;
  @ViewChild('chart8') _chart8: PlotlyChartData;
  @ViewChild('chart9') _chart9: PlotlyChartData;
  @ViewChild('chart10') _chart10: PlotlyChartData;

  constructor(private queryService: JSONQueryService, private _router: Router) { };

  ngOnInit() {

    this._chart1.plotQuery('rate_inclusive', 'hlt1_ByRegex_Hlt1.*', 'lhcb-2017-patches', 21, ['options=RateTest_0x1706', ],
      function(data) {
        this.summaryClickHandler('chart1', data);
      }.bind(this));
    this._chart2.plotQuery('rate', 'hlt2_decisions_Total', 'lhcb-2017-patches', 21, ['options=RateTest_0x1706', ],
      function(data) {
        this.summaryClickHandler('chart2', data);
      }.bind(this));


    this._chart3.plotQuery('rate_inclusive', 'hlt1_ByRegex_Hlt1.*', 'lhcb-2018-patches', 21, ['options=RateTest_0x1707', ],
      function(data) {
        this.summaryClickHandler('chart3', data);
      }.bind(this));
    this._chart4.plotQuery('rate', 'hlt2_decisions_Total', 'lhcb-2018-patches', 21, ['options=RateTest_0x1707', ],
      function(data) {
        this.summaryClickHandler('chart4', data);
      }.bind(this));


    this._chart5.plotQuery('total (s)', 'HLTPerfHlt1_EVENT+LOOP', 'lhcb-2017-patches', 21, ['options=HLT1_throughput', ],
      function(data) {
        this.summaryClickHandler('chart5', data);
      }.bind(this));
    this._chart6.plotQuery('total (s)', 'HLTPerfHlt2_EVENT+LOOP', 'lhcb-2017-patches', 21, ['options=HLT2_throughput', ],
      function(data) {
        this.summaryClickHandler('chart6', data);
      }.bind(this));



    this._chart7.plotQuery('total (s)', 'HLTPerfHlt1_EVENT+LOOP', 'lhcb-2018-patches', 21, ['options=HLT1_throughput', ],
      function(data) {
        this.summaryClickHandler('chart7', data);
      }.bind(this));
    this._chart8.plotQuery('total (s)', 'HLTPerfHlt2_EVENT+LOOP', 'lhcb-2018-patches', 21, ['options=HLT2_throughput', ],
      function(data) {
        this.summaryClickHandler('chart8', data);
      }.bind(this));



    this._chart9.plotQuery('total (s)', 'HLTPerfHlt1_EVENT+LOOP', 'lhcb-master', 21, ['options=HLT1_throughput', ],
      function(data) {
        this.summaryClickHandler('chart9', data);
      }.bind(this));
    this._chart10.plotQuery('total (s)', 'HLTPerfHlt2_EVENT+LOOP', 'lhcb-master', 21, ['options=HLT2_throughput', ],
      function(data) {
        this.summaryClickHandler('chart10', data);
      }.bind(this));


  };

  summaryClickHandler(chartID, data) {
    var job_time = data.points[0].x;
    var job_date = String(job_time).split(' ')[0];

    var app_version = '';
    var app_options = '';
    var app_name = '';

    switch (chartID) {
      case 'chart1':
        app_version='lhcb-2017-patches';
        app_options='RateTest_0x1706';
        app_name = 'MOORE';
        break;
      case 'chart2':
        app_version='lhcb-2017-patches';
        app_options='RateTest_0x1706';
        app_name = 'MOORE';
        break;
      case 'chart3':
        app_version='lhcb-2018-patches';
        app_options='RateTest_0x1707';
        app_name = 'MOORE';
        break;
      case 'chart4':
        app_version='lhcb-2018-patches';
        app_options='RateTest_0x1707';
        app_name = 'MOORE';
        break;
      case 'chart5':
        app_version='lhcb-2017-patches';
        app_options='HLT1_throughput';
        app_name = 'MOOREONLINE';
        break;
      case 'chart6':
        app_version='lhcb-2017-patches';
        app_options='HLT2_throughput';
        app_name = 'MOOREONLINE';
        break;
      case 'chart7':
        app_version='lhcb-2018-patches';
        app_options='HLT1_throughput';
        app_name = 'MOOREONLINE';
        break;
      case 'chart8':
        app_version='lhcb-2018-patches';
        app_options='HLT2_throughput';
        app_name = 'MOOREONLINE';
        break;
      case 'chart9':
        app_version='lhcb-master';
        app_options='HLT1_throughput';
        app_name = 'MOOREONLINE';
        break;
      case 'chart10':
        app_version='lhcb-master';
        app_options='HLT2_throughput';
        app_name = 'MOOREONLINE';
        break;

      default:
        return;
    }

    this.queryService.getJobsFromDate(app_name, app_version, app_options, job_date ).subscribe( jobIDs => {

      var jobID = this.getJobID(jobIDs.results, job_time);

      this.redirectTo(chartID, jobID);
    });

  };

  getJobID(jobIDs: JobId[]=[], jobTime: string): number {
    var actual = new Date(jobTime).valueOf();

    if( jobIDs.length == 1 ) {
      return jobIDs[0].id;
    } else {
      var diff_data = {};
      for( var i=0; i< jobIDs.length; i++ ) {
        var jobDate = new Date(jobIDs[i].date).valueOf();
        var time_diff  = Math.abs(jobDate - actual);
        diff_data[time_diff] = jobIDs[i].id;
      }
      var min = 1000000;
      for( var diff in diff_data ) {
        if( parseInt(diff) < min ) {
          min = parseInt(diff);
        }
      }
      return diff_data[min];
    }
  };

  redirectTo(chartID: string, jobID: number) {

    var url='';

    switch (chartID) {
      case 'chart1':
        url='hlt/rate/?jid='+String(jobID);
        break;
      case 'chart2':
        url='hlt/rate/?jid='+String(jobID);
        break;
      case 'chart3':
        url='hlt/rate/?jid='+String(jobID);
        break;
      case 'chart4':
        url='hlt/rate/?jid='+String(jobID);
        break;
      case 'chart5':
        url='hlt/timing/?jid='+String(jobID);
        break;
      case 'chart6':
        url='hlt/timing/?jid='+String(jobID);
        break;
      case 'chart7':
        url='hlt/timing/?jid='+String(jobID);
        break;
      case 'chart8':
        url='hlt/timing/?jid='+String(jobID);
        break;
      case 'chart9':
        url='hlt/timing/?jid='+String(jobID);
        break;
      case 'chart10':
        url='hlt/timing/?jid='+String(jobID);
        break;
    
      default:
        return;
    }

    console.log("Redirecting to: " + url);

    const queryParams: Params = {'jid': jobID};
    this._router.navigate([url.split('?')[0]], {queryParams: queryParams} );
  }

};
