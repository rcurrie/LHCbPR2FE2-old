
export interface JSONResult {
    id: number;
    value: object;
};

export interface AllJobResults {
    results: JSONResult[];
};

export interface JSONTrendData {
    results: object;
};

export interface UniqueBuildIDs {
    results: string[];
};

export class OptionsData {
    content: string;
    description: string;
    id: number;
};

export interface FilteredOptions {
    results: OptionsData[];
};

export class PlatformData {
    content: string;
};

export interface FilteredPlatforms {
    results: PlatformData[];
};

export class JobId {
    date: string;
    id: number;
};

export interface JobsIDs {
    results: JobId[];
};

export class jobValues {
    job: jobData;
    value: string;
};

export class resultValues {
    id: number;
    name: string;
    dtype: string;
    description: string;
    thresholds: object[];
    jobvalues: jobValues[];
};

export interface perfData {
    next: string | null;
    results: resultValues[];
    count: number;
};

export class ExecutableDescription {
    id: number;
    name: string;
    content: string;
};

export class JobDescriptions {
    setup_project: string | null;
    option: OptionDescription;
    executable: ExecutableDescription;
};

// NB: This may be replaced with something else more appropriate in the shared framework eventually
export class ApplicationVersion {
    id: number;
    application: object;
    version: string;
    is_nightly: boolean;
    job_descriptions: JobDescriptions[];
};

export class OptionDescription {
    id: number;
    content: string;
    description: string;
    thresholds: object[];
};

export class JobDescription {
    application_version: ApplicationVersion;
    setup_project: object | null;
    option: OptionDescription;
};

export class HostDescription {
    id: number;
    hostname: string;
    cpu_info: string;
    memory_info: string;
};

export class PlatformDescription {
    content: string;
};

export interface jobData {
    id: number;
    resource_uri: string;
    job_description: JobDescription;
    host: HostDescription;
    platform: PlatformDescription;
    time_start: string;
    time_end: string;
    status: number;
    is_success: boolean;
};

export interface resultObj {
    description: string;
    value: any;
    name: string;
}

export interface rawResultData {
    Integer: resultObj[];
    JSON: resultObj[];
    Float: resultObj[];
    String: resultObj[];
    File: resultObj[];
}

export interface justJobData {
    id: number;
    results: rawResultData;
}
