import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PRJobsFileViewerComponent } from './jobs-file-viewer.component';

export const routes: Routes = [
  { path: '', component: PRJobsFileViewerComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PRJobsFileViewerRoutingModule { }
