import { Component, Input, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';
import { JSONQueryService } from '../../../core/services/json-query/json-query.service';
import { FormGroup, FormBuilder, NgModel } from '@angular/forms';
import { SingleJobSelectorState } from '../../../core/services/json-query/single-job-selector-state';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'single-job-selector',
  templateUrl: './single-job-selector.component.html'
})
export class SingleJobSelectorComponent implements OnInit {

    possibleBuildIDs: string[] = [];
    possibleOptions: object[] = [];
    possiblePlatforms: object[] = [];
    possibleJobs: object[] = [];
    selectionState: SingleJobSelectorState = new SingleJobSelectorState();
    jobQueryForm: FormGroup;
    dontReset: boolean = false;
    routeSubscription: Subscription;
    @ViewChild('ctrl') jobIdModel: NgModel;
    @Input() appName: string;
    @Input() executable: string;

    @Output() selectedJob: EventEmitter<number> = new EventEmitter<number>();
    @Output() get fixedSelectionState(): SingleJobSelectorState {
        var state = new SingleJobSelectorState();
        state.app_id = this.appName;
        state.build_id = this.selectionState.build_id;
        state.option_id = this.selectionState.option_id;
        state.platform_id = this.selectionState.platform_id;
        state.job_id = this.selectionState.job_id;
        state.exec_id = this.selectionState.exec_id;
        return state;
    };

    constructor(private _queryService: JSONQueryService, private _fb: FormBuilder, private _activatedRoute: ActivatedRoute, private _router: Router) {

    };

    ngOnInit() {

        console.log(this.appName);
        console.log(this.executable);

        this.jobQueryForm = this._fb.group({
            'build_id': '',
            'option_id': '',
            'platform_id': '',
            'job_id': 0
        });

        if( this.appName === undefined ) {
            console.log("Failed to init job-selector with an appName.");
            return;
        }

        if( this.executable !== undefined ) {
            this.selectionState.exec_id = this.executable;
            console.log(this.executable);
        }

        this.selectionState.app_id = this.appName;

        this._queryService.getBuildIDs(this.selectionState).subscribe( buildIDs => {
            this.possibleBuildIDs = buildIDs.results;
        });

        this.jobQueryForm.controls['build_id'].valueChanges.subscribe((value) => {
            if(!this.dontReset) this.selectionState.resetBuild();
            if( !value ) return;
            this._queryService.getFilteredOptions(this.selectionState).subscribe( optionIDs => {
                this.possibleOptions = optionIDs.results;
            });
        });

        this.jobQueryForm.controls['option_id'].valueChanges.subscribe((value) => {
            if(!this.dontReset) this.selectionState.resetOption();
            if( !value ) return;
            this._queryService.getFilteredPlatforms(this.selectionState).subscribe( platformIds => {
                this.possiblePlatforms = platformIds.results;
            });
        });

        this.jobQueryForm.controls['platform_id'].valueChanges.subscribe((value) => {
            if(!this.dontReset) this.selectionState.resetPlatform();
            if( !value ) return;
            this._queryService.getJobIds(this.selectionState).subscribe( jobIds => {
                this.possibleJobs = jobIds.results;
            });
        });

        this.jobQueryForm.controls['job_id'].valueChanges.subscribe((value) => {
            this.selectedJob.emit(value);
            this.updateHistory();
        });

        this.subscribeToQueryParams();
    };

    subscribeToQueryParams(ignoreNavigate: boolean = false) {
        this.routeSubscription = this._activatedRoute.queryParams.skip(ignoreNavigate ? 2 : 0).subscribe(params => {
            var jobId = 'jid' in params ? params['jid'] : undefined;
            if( jobId ) this.getJobState(jobId);
        });       
    }

    ngAfterViewChecked() {
        if(this.dontReset) {
            this.dontReset = false;
        }
    }

    updateHistory() {
        this.routeSubscription.unsubscribe();
        const queryParams: Params = this.selectionState.toQueryParms();
        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: queryParams });
        this.subscribeToQueryParams(true);
    };

    getJobState(job_id: number) {
        this.selectionState.exec_id = this.executable;
        this.selectionState.app_id = this.appName;
        this._queryService.getJobData(job_id).subscribe( JobData => {
            var platform = JobData.platform.content;
            var option = JobData.job_description.option.description;
            var build = JobData.job_description.application_version.version.split('.')[0];
            var date = JobData.time_start;

            var executable = JobData.job_description.application_version.job_descriptions[0].executable.name;

            /*
            console.log(JobData.job_description.application_version.job_descriptions[0]);

            if( JobData.job_description.application_version.job_descriptions.length != 1 ) {
                console.log("WARNING: multiple job descriptions for this jobid.");
                for( var i=0; i< JobData.job_description.application_version.job_descriptions.length; i++ ) {
                    console.log("Name: " + JobData.job_description.application_version.job_descriptions[i].executable.name);
                }
            }

            console.log("Found:");
            console.log("Platform: " + platform);
            console.log("Option: " + option);
            console.log("Build: " + build);
            console.log("Date: " + date);
            console.log("Executable: " + executable);
            */

            this.dontReset = true;

            this.selectionState.build_id = build;
            this.selectionState.platform_id = platform;
            this.selectionState.option_id = option;
            this.selectionState.job_id = job_id;
            

        });
    };

    getJobIDFromDate(this_date: string) {
        //TODO requires supportin the BE to continue
    };

};