import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-compare-values-chart',
  templateUrl: './compare-values-chart.component.html'
})
export class PRCompareValuesChartComponent {
  @Input() chartLegend = true;
  @Input() chartType = 'line';
  @Input() chartData: Array<any> = [];
  @Input() chartLabels: Array<string> = [];
  @Input() chartOptions: any = {
    responsive: true,
    title: {
      display: true,
      text: 'Untitled'
    }
  };
}
