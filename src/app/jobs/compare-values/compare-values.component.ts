import { Component, Input } from '@angular/core';

import { PRCompareValuesChartComponent } from '../compare-values-chart/compare-values-chart.component';

import { PRMeasure } from '../../shared/classes/measure';

@Component({
  selector: 'app-compare-values',
  templateUrl: './compare-values.component.html'
})
export class PRCompareValuesComponent {
  @Input() public data: PRMeasure;
  public modalOpened = false;
  lineChartLegend = true;
  lineChartType = 'line';

  lineChartOptions: any = {
    responsive: true
  };

  lineChartData: Array<any> = [
    // Values for debugging
    // { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A', fill: false },
    // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B', fill: false },
    // { data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C', fill: false }
  ];

  lineChartLabels: Array<string> = [
    // Values for debugging
    // 'January', 'February', 'March', 'April', 'May', 'June', 'July'
  ];

  plot() {
    // If it's a number, we can plot it
    if ((this.data.dtype === 'Float' || this.data.dtype === 'Integer') && this.data.jobvalues.length) {
      const newDataSets: Array<any> = [];
      const newLabels: Array<string> = [];
      const newDataSet = {
        data: [],
        label: this.data.name,
        fill: false,
        showLine: true
      };

      for (const el of this.data.jobvalues) {
        newDataSet.data.push(el.value);
        newLabels.push(el.job.job_description.application_version.version + el.job.platform.content);
      }

      newDataSets.push(newDataSet);
      this.lineChartData = newDataSets;
      this.lineChartLabels = newLabels;
    }
  }
}
