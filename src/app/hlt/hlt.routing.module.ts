import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PRHLTCPUPerformance } from './hlt-performance-cpu/hlt-performance-cpu.component';
import { PRHLTMemPerformance } from './hlt-performance-mem/hlt-performance-mem.component';
import { PRHLTSummaryComponent } from './hlt-summary/hlt-summary.component';
import { PRHLTRateTest } from './hlt-ratetest/hlt-ratetest.component';
import { PRHLTimingTest } from './hlt-timingtest/hlt-timingtest.component';

export const routes: Routes = [
  { path: 'summary', component: PRHLTSummaryComponent },
  { path: 'rate', component: PRHLTRateTest },
  { path: 'timing', component: PRHLTimingTest },
  { path: 'perf-cpu', component: PRHLTCPUPerformance },
  { path: 'perf-mem', component: PRHLTMemPerformance },
  { path: '', redirectTo: 'summary', pathMatch: 'full' },
  { path: '**', redirectTo: 'summary' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PRHLTRoutingModule { };
