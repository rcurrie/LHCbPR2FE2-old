import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { PR_API_MAP } from '../../shared/api-map';
import { PRIBasicApiResponse } from '../../shared/interfaces/basic-api-response';
import { PRIVersionsApiResponse } from '../../shared/interfaces/versions-api-response';
import { PRJobsDataResponse } from '../../shared/classes/jobs-data-response';
import { PRJobsComparisonResponse } from '../../shared/classes/jobs-comparison-response';

@Injectable({
  providedIn: 'root'
})
export class PRSearchJobsService {
  constructor(private _http: HttpClient) { }

  getApplications(): Observable<PRIBasicApiResponse[]> {
    return this._http.get<PRIBasicApiResponse[]>(PR_API_MAP.url_active_applications);
  }

  getVersionsForApplicaton(id: number, params?: {}): Observable<PRIVersionsApiResponse[]> {
    console.log('DEBUG: getVersionsForApplicaton called with app id: ' + id);
    return this._http.get<PRIVersionsApiResponse[]>(
      `${PR_API_MAP.url_active_applications}${id}/versions/`,
      { params: params }
    );
  }

  getOptionsForApplicaton(id: number): Observable<PRIBasicApiResponse[]> {
    console.log('DEBUG: getOptionsForApplicaton called with app id: ' + id);
    return this._http.get<PRIBasicApiResponse[]>(`${PR_API_MAP.url_active_applications}${id}/options/`);
  }

  getExecutablesForApplicaton(id: number): Observable<PRIBasicApiResponse[]> {
    console.log('DEBUG: getExecutablesForApplicaton called with app id: ' + id);
    return this._http.get<PRIBasicApiResponse[]>(`${PR_API_MAP.url_active_applications}${id}/executables/`);
  }

  getPlatformsForApplicaton(id: number): Observable<PRIBasicApiResponse[]> {
    console.log('DEBUG: getPlatformsForApplicaton called with app id: ' + id);
    return this._http.get<PRIBasicApiResponse[]>(`${PR_API_MAP.url_active_applications}${id}/platforms/`);
  }

  getHostsForApplicaton(id: number): Observable<PRIBasicApiResponse[]> {
    console.log('DEBUG: getHostsForApplicaton called with app id: ' + id);
    return this._http.get<PRIBasicApiResponse[]>(`${PR_API_MAP.url_active_applications}${id}/hosts/`);
  }

  getJobsData(params: {}): Observable<PRJobsDataResponse> {
    console.log('DEBUG: getJobsData called with params: ' + JSON.stringify(params));
    return this._http.get<PRJobsDataResponse>(
      `${PR_API_MAP.url_search_jobs}`,
      { params: params }
    );
  }

  getCompareJobs(params: {}): Observable<PRJobsComparisonResponse> {
    console.log('DEBUG: getCompareJobs called with params: ' + JSON.stringify(params));
    return this._http.get<PRJobsComparisonResponse>(
      `${PR_API_MAP.url_compare}`,
      { params: params }
    );
  }

  getROOTService(service: string, params?: {}): Observable<any[]> {
    console.log('DEBUG: getROOTService called with service: "' + service + '" and params: ' + JSON.stringify(params));
    console.log(`DEBUG:   Full URL: ${PR_API_MAP.url_root}${service}/`);
    return this._http.get<any[]>(
      `${PR_API_MAP.url_root}${service}/`,
      { params: params }
    );
  }
}
