import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClarityModule } from '@clr/angular';

import { PRHLTRoutingModule } from './hlt.routing.module';
import { PRSharedModule } from '../shared/shared.module';
import { PlotlyChartData } from '../shared/components/plotly-chart/plotly-chart.component';

import { PRHLTComponent } from './hlt.component';

import { JSONTableService } from './json-table/json-table.component';
import { HLTCompareValuesComponent } from './compare-values/compare-values.component';

import { PRHLTSummaryComponent } from './hlt-summary/hlt-summary.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PRHLTCPUPerformance } from './hlt-performance-cpu/hlt-performance-cpu.component';
import { PRHLTMemPerformance } from './hlt-performance-mem/hlt-performance-mem.component';
import { PRHLTRateTest } from './hlt-ratetest/hlt-ratetest.component';
import { JSONFilterTable } from './json-filter-table/json-filter-table.component';
import { PRHLTimingTest } from './hlt-timingtest/hlt-timingtest.component';
import { SimpleTableService } from './simple-table/simple-table.component';

import { PlotlyModule } from 'angular-plotly.js';

import { SingleJobSelectorComponent } from '../shared/components/single-job-selector/single-job-selector.component';

@NgModule({
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,
    PRSharedModule,
    PRHLTRoutingModule,
    PlotlyModule
  ],
  declarations: [
    PRHLTSummaryComponent,
    PRHLTCPUPerformance,
    PRHLTMemPerformance,
    PRHLTRateTest,
    PRHLTimingTest,
    PRHLTComponent,
    JSONTableService,
    SimpleTableService,
    JSONFilterTable,
    SingleJobSelectorComponent,
    HLTCompareValuesComponent,
    PlotlyChartData
  ],
  providers: [],
  exports: []
})
export class PRHLTModule { };
