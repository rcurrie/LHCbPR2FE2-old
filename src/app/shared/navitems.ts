export const NAVIGATION_ITEMS = [
  {
    id: 'jobs',
    label: 'Jobs',
    routerLink: '/jobs',
    activeExact: false,
    subgroup: [
      { id: 'jobs-details', label: 'Details', routerLink: '/jobs/details', activeExact: true },
      { id: 'jobs-viewer', label: 'ROOT file viewer', routerLink: '/jobs/viewer', activeExact: true }
    ]
  },
  {
    id: 'gauss',
    label: 'Gauss',
    routerLink: '/gauss',
    activeExact: false,
    subgroup: [
      { id: 'gauss-validan', label: 'Validation analysis', routerLink: '/gauss/validan', activeExact: true },
      { id: 'gauss-numan', label: 'Numerical analysis', routerLink: '/gauss/numan', activeExact: true },
      { id: 'gauss-detime', label: 'Detailed timing', routerLink: '/gauss/detime', activeExact: true },
      { id: 'gauss-hardxs', label: 'Hadronic analysis', routerLink: '/gauss/hadrxs', activeExact: true }
    ]
  },
  {
    id: 'geant4',
    label: 'Geant4',
    routerLink: '/geant',
    activeExact: false,
    subgroup: [
      { id: 'g4-hadronic', label: 'Hadronic', routerLink: '/geant/hadronic', activeExact: true },
      { id: 'g4-calo', label: 'Calorimeter', routerLink: '/geant/calo', activeExact: true },
      { id: 'g4-msc', label: 'Multiple scattering', routerLink: '/geant/msc', activeExact: true },
      { id: 'g4-richtb', label: 'RICH test beam', routerLink: '/geant/richtb', activeExact: true }
    ]
  },
  {
    id: 'hlt',
    label: 'HLT',
    routerLink: '/hlt',
    activeExact: false,
    subgroup: [
      { id: 'hlt-summary', label: 'Summary', routerLink: '/hlt/summary', activeExact: true },
      { id: 'hlt-rate', label: 'RateTest', routerLink: '/hlt/rate', activeExact: true },
      { id: 'hlt-timing', label: 'TimingTest', routerLink: '/hlt/timing', activeExact: true },
      { id: 'hlt-cpu-performance', label: 'CPU Performance', routerLink: '/hlt/perf-cpu', activeExact: true },
      { id: 'hlt-mem-performance', label: 'Mem Performance', routerLink: '/hlt/perf-mem', activeExact: true },
    ]
  },
  {
    id: 'brunel',
    label: 'Brunel',
    routerLink: '/brunel',
    activeExact: false,
    subgroup: [
      { id: 'mini-brunel', label: 'MiniBrunel', routerLink: '/brunel/minibrunel', activeExact: true }
    ]
  },
  {
    id: 'faq',
    label: 'FAQ',
    routerLink: '/faq',
    activeExact: false,
    subgroup: [
      { id: 'faq', label: 'FAQ', routerLink: '/faq', activeExact: true }
    ]
  }
];
