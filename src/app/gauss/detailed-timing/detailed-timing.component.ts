import { Component, ViewChild, OnInit } from '@angular/core';

import { PRSearchJobsService } from '../../core/services/search-jobs.service';
import { PRSelectionStateService } from '../../core/services/selection-state.service';
import { PRJobsComparisonResponse } from '../../shared/classes/jobs-comparison-response';
import { PRIJobsComparisonParams } from '../../shared/interfaces/jobs-comparison-params';

import { PRISelectionEvent } from '../../shared/interfaces/selection-event';
import { PRJobFiltersPresets } from '../../shared/classes/job-filters-presets';
import { PRSelectJobsComponent } from '../../search-jobs/select-jobs/select-jobs.component';

declare var JSROOT: any;

@Component({
  selector: 'app-detailed-timing',
  templateUrl: './detailed-timing.component.html'
})
export class PRDetailedTimingComponent implements OnInit {
  @ViewChild(PRSelectJobsComponent) selectJobsChild: PRSelectJobsComponent;
  private _jobsIDs: number[] = [];
  jobFiltersPresets: PRJobFiltersPresets;
  comparedJobsData: PRJobsComparisonResponse;
  hideTimingPlot = true;
  hideRatioPlot = true;
  hideTrendPlot = true;
  hideParticlesPlot = true;

  constructor(private _jobs: PRSearchJobsService, private _state: PRSelectionStateService) { }

  ngOnInit() {
    // Preset all to Gauss and options to DetailedTiming
    this.jobFiltersPresets = new PRJobFiltersPresets(4, [21]);
  }

  // Triggered by: SearchJobs when selection criteria change
  // Triggers: SelectJobs to refresh the list of jobs
  onSearchSelectionChanged() {
    this.selectJobsChild.selectedItems = [];
    this.selectJobsChild.refreshDatagrid();
  }

  // Triggered by: SelectJobs when 'Analyse' button is clicked
  // Triggers: SelectJobs and SelectValues
  onAnalyseJobs(payload?: PRISelectionEvent) {
    if (payload.data.length) {
      const params: PRIJobsComparisonParams = {
       'ids': payload.data.slice(),
       'contains': 'detailed_timing_in_volumes'
      };

      this._jobs.getCompareJobs(this._state.getCompareJobsHttpParams(params))
          .subscribe((res) => {
            this.comparedJobsData = res;
            this._processDataAndPlot();
          });
    }
  }

  // Process jobs' data and display plots
  private _processDataAndPlot() {
    const jobs_names = [];
    const summary_data_values = [];
    const summary_data_lables = [];

    for (let i = 0; i < this.comparedJobsData.results[0].jobvalues.length; i++) {
      const job = this.comparedJobsData.results[0].jobvalues[i].job;
      const job_name = 'Job' + job.id
                     + '-' + job.job_description.application_version.version
                     + '-' + job.platform.content;

      jobs_names.push(job_name);

      const job_data = JSON.parse(this.comparedJobsData.results[0].jobvalues[i].value);
      const data_values = [];
      const summary_block = job_data['summary_of_total_time_in_each_volume'];

      for (let key = 0; key < summary_block.length; key++) {
        data_values.push(summary_block[key][1]);
        // Lables are the same, so save them only once
        if (i === 0) {
          summary_data_lables.push(summary_block[key][0]);
        }
      }
      summary_data_values.push(data_values);
    }

    const hrtplot_params = {
     'jobnames': jobs_names.join(','),
     'values': summary_data_values.join(';'),
     'labels': summary_data_lables.join(','),
     'title': 'Total time in each detector volume',
     'xaxis': 'detector',
     'yaxis': 'seconds'
    };

    if (this.comparedJobsData.results[0].jobvalues.length > 1) {
      hrtplot_params['ratio'] = 1;
      hrtplot_params['trend'] = 1;
    }

    // Plot summary timing
    this._plotSummaryTiming(hrtplot_params);

    // Process and plot particles timing
    this._plotParticlesTiming(jobs_names);
  }

  private _plotSummaryTiming(params = {}) {
    // Request the plots from the ROOT backend and plot them with JSROOT
    this._jobs.getROOTService('hrtplot', params)
        .subscribe((res) => {
          this.selectJobsChild.visible = false;

          // Draw the main timing plot
          this.hideTimingPlot = false;
          setTimeout(() => {
            document.getElementById('detime-container-timing-plot').style.height = res['result'].fCh + 'px';
            JSROOT.redraw('detime-container-timing-plot', JSROOT.JSONR_unref(res['result']));
          });

          // Draw the ration plot if exists
          if (res['ratio']) {
            this.hideRatioPlot = false;
            setTimeout(() => {
              document.getElementById('detime-container-ratio-plot').style.height = res['ratio'].fCh + 'px';
              JSROOT.redraw('detime-container-ratio-plot', JSROOT.JSONR_unref(res['ratio']));
            });
          }

          // Draw the trend plot if exists
          if (res['trend']) {
            this.hideTrendPlot = false;
            setTimeout(() => {
              document.getElementById('detime-container-trend-plot').style.height = res['trend'].fCh + 'px';
              JSROOT.redraw('detime-container-trend-plot', JSROOT.JSONR_unref(res['trend']));
            });
          }
        });
  }

  private _plotParticlesTiming(jobs_names = []) {
    if (!jobs_names.length) { return; }

    const particles_timing_data = this._processParticleData();
    const list_of_dets = [];
    const list_of_particles = [];
    const list_of_values = [];

    for (const key_det in particles_timing_data) {
      if (particles_timing_data.hasOwnProperty(key_det)) {
        list_of_dets.push(key_det);
        const tmp_particles = [];
        const tmp_values = [];

        for (const key_part in particles_timing_data[key_det]) {
          if (particles_timing_data[key_det].hasOwnProperty(key_part)) {
            tmp_particles.push(key_part);
            tmp_values.push(particles_timing_data[key_det][key_part][jobs_names[0]]);
          }
        }

        list_of_particles.push(tmp_particles);
        list_of_values.push(tmp_values);
      }
    }

    const text2dhist_params = {
      'values': list_of_values.join(';'),
      'xlabels': list_of_dets.join(';'),
      'ylabels': list_of_particles.join(';'),
      'title': 'Timing of particle groups per detector for ' + jobs_names[0]
    };

    this._jobs.getROOTService('text2dhist', text2dhist_params)
        .subscribe((res) => {
          // Draw the particles timing table
          this.hideParticlesPlot = false;
          setTimeout(() => {
            console.log(res['result'].fCh);
            document.getElementById('detime-container-particles-plot').style.height = res['result'].fCh + 'px';
            JSROOT.redraw('detime-container-particles-plot', JSROOT.JSONR_unref(res['result']));
          });
        });
  }

  private _processParticleData() {
    const table_data = {};

    // Define groups of particles
    const particle_groups_names = [
      'e',
      'pi',
      'K',
      'mu',
      'gamma',
      'opticalphoton',
      'proton',
      'neutron',
      'deuteron',
      'pe-',
      'alpha',
      'other'
    ];

    // Assign particles to groups
    const particle_groups = {
      'e+': particle_groups_names[0],
      'e-': particle_groups_names[0],
      'pi+': particle_groups_names[1],
      'pi-': particle_groups_names[1],
      'kaon+': particle_groups_names[2],
      'kaon-': particle_groups_names[2],
      'mu+': particle_groups_names[3],
      'mu-': particle_groups_names[3],
      'gamma': particle_groups_names[4],
      'opticalphoton': particle_groups_names[5],
      'proton': particle_groups_names[6],
      'neutron': particle_groups_names[7],
      'deuteron': particle_groups_names[8],
      'pe-': particle_groups_names[9],
      'alpha': particle_groups_names[10]
    };

    // Create and populate the data structure to simplify plotting:
    // Detector -> Particle -> JobName -> value
    // - Single job only, no comparison across jobs required
    // - Group particles of interest
    const job = this.comparedJobsData.results[0].jobvalues[0].job;
    const job_name = 'Job' + job.id
                   + '-' + job.job_description.application_version.version
                   + '-' + job.platform.content;

    const job_data = JSON.parse(this.comparedJobsData.results[0].jobvalues[0].value);
    const block_name_prefix = 'timing_per_particle_type_in_';

    // Loop over detectors records
    for (const key_det in job_data) {
      if (key_det.startsWith(block_name_prefix)) {
        const det_name = key_det.replace(block_name_prefix, '');
        // Ignore the 'all' category - it equals to summary
        if (det_name === 'all') {
          continue;
        }

        if (typeof table_data[det_name] === 'undefined') {
          table_data[det_name] = Object();
        }

        // Loop over the particle groups names and create their records
        // This trick helps to maintain order across detectors
        // for (var key_part in particle_groups_names) {
        for (let key_part = 0; key_part < particle_groups_names.length; key_part++) {
          table_data[det_name][particle_groups_names[key_part]] = Object();
          table_data[det_name][particle_groups_names[key_part]][job_name] = 0.;
        }

        // Loop over particles
        // for (var key_part in job_data[key_det]) {
        for (let key_part = 0; key_part < job_data[key_det].length; key_part++) {
          const part = job_data[key_det][key_part];
          // Ignore the total time in this detector - already in the summary
          if (part[0] === det_name) {
            continue;
          }

          let part_name = '';
          if (part[0] in particle_groups) {
            part_name = particle_groups[part[0]];
          } else {
            part_name = particle_groups_names[11];
          }

          table_data[det_name][part_name][job_name] += Number(part[1]);
        }
      }
    }

    return table_data;
  }

  // Hide and clear the plots and show the search results grid
  onReturnToSearch() {
    // Clear and hide the plots
    this.hideTimingPlot = true;
    this.hideRatioPlot = true;
    this.hideTrendPlot = true;
    this.hideParticlesPlot = true;

    // Show search results
    this.selectJobsChild.visible = true;
  }
}
