import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { ClarityModule } from '@clr/angular';

import { PRAppRoutingModule } from './app.routing.module';
import { PRCoreModule } from './core/core.module';
import { PRUIModule } from './ui/ui.module';
import { PRJobsModule } from './jobs/jobs.module';
import { PRGaussModule } from './gauss/gauss.module';
import { PRGeantModule } from './geant/geant.module';
import { PRHLTModule } from './hlt/hlt.module';
import { PRBrunelModule } from './brunel/brunel.module';
import { PRDetailedTimingModule } from './gauss/detailed-timing/detailed-timing.module';

import { AppComponent } from './app.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ClarityModule.forRoot(),
    PRAppRoutingModule,
    PRCoreModule,
    PRUIModule,
    PRJobsModule,
    PRGaussModule,
    PRGeantModule,
    PRHLTModule,
    PRBrunelModule,
    PRDetailedTimingModule
  ],
  declarations: [
    AppComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
