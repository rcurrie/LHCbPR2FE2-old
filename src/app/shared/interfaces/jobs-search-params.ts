export interface PRIJobsSearchParams {
  app_id?: number;
  pagesize?: number;
  page?: number;
  versions?: number[];
  options?: number[];
  executables?: number[];
  platforms?: number[];
  hosts?: number[];
}
