import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClarityModule } from '@clr/angular';

import { PRDetailedTimingRoutingModule } from './detailed-timing.routing.module';
import { PRSharedModule } from '../../shared/shared.module';
import { PRSearchJobsModule } from '../../search-jobs/search-jobs.module';

import { PRDetailedTimingComponent } from './detailed-timing.component';

@NgModule({
  imports: [
    CommonModule,
    ClarityModule,
    PRDetailedTimingRoutingModule,
    PRSharedModule,
    PRSearchJobsModule
  ],
  declarations: [
    PRDetailedTimingComponent
  ],
  exports: [
    PRDetailedTimingComponent
  ]
})
export class PRDetailedTimingModule { }
