import { Injectable } from '@angular/core';

import { PRSelectionState } from '../../shared/classes/selection-state';
import { PRIBasicApiResponse } from '../../shared/interfaces/basic-api-response';
import { PRIVersionsApiResponse } from '../../shared/interfaces/versions-api-response';
import { PRJobsDataResponse } from '../../shared/classes/jobs-data-response';
import { PRIJobsComparisonParams } from '../../shared/interfaces/jobs-comparison-params';
import { PRIJobsSearchParams } from '../../shared/interfaces/jobs-search-params';

@Injectable({
  providedIn: 'root'
})
export class PRSelectionStateService {
  private _state: PRSelectionState = new PRSelectionState();

  constructor() { }

  getAppID(): number {
    return(this._state.app_id);
  }

  setAppID(id: number) {
    this._state.app_id = id;
  }

  getWithNightly(): boolean {
    return(this._state.with_nightly);
  }

  setWithNightly(val: boolean) {
    this._state.with_nightly = val;
  }

  getCheckSpecific(): boolean {
    return(this._state.check_specific);
  }

  setCheckSpecific(val: boolean) {
    this._state.check_specific = val;
  }

  getNightlyVersionNumber(): number {
    return(this._state.nightly_version_number);
  }

  setNightlyVersionNumber(val: number) {
    this._state.nightly_version_number = val;
  }

  // Versions
  getUnfilteredVersions(): PRIVersionsApiResponse[] {
    return(this._state.unfiltered_versions.slice());
  }

  setUnfilteredVersions(vals: PRIVersionsApiResponse[]) {
    this._state.unfiltered_versions = vals.slice();
  }

  getFilteredVersions(): number[] {
    return(this._state.filtered_versions.slice());
  }

  setFilteredVersions(vals: number[]) {
    this._state.filtered_versions = vals.slice();
  }

  // Options
  getUnfilteredOptions(): PRIBasicApiResponse[] {
    return(this._state.unfiltered_options.slice());
  }

  setUnfilteredOptions(vals: PRIBasicApiResponse[]) {
    this._state.unfiltered_options = vals.slice();
  }

  getFilteredOptions(): number[] {
    return(this._state.filtered_options.slice());
  }

  setFilteredOptions(vals: number[]) {
    this._state.filtered_options = vals.slice();
  }

  // Executables
  getUnfilteredExecutables(): PRIBasicApiResponse[] {
    return(this._state.unfiltered_executables.slice());
  }

  setUnfilteredExecutables(vals: PRIBasicApiResponse[]) {
    this._state.unfiltered_executables = vals.slice();
  }

  getFilteredExecutables(): number[] {
    return(this._state.filtered_executables.slice());
  }

  setFilteredExecutables(vals: number[]) {
    this._state.filtered_executables = vals.slice();
  }

  // Hosts
  getUnfilteredHosts(): PRIBasicApiResponse[] {
    return(this._state.unfiltered_hosts.slice());
  }

  setUnfilteredHosts(vals: PRIBasicApiResponse[]) {
    this._state.unfiltered_hosts = vals.slice();
  }

  getFilteredHosts(): number[] {
    return(this._state.filtered_hosts.slice());
  }

  setFilteredHosts(vals: number[]) {
    this._state.filtered_hosts = vals.slice();
  }

  // Platforms
  getUnfilteredPlatforms(): PRIBasicApiResponse[] {
    return(this._state.unfiltered_platforms.slice());
  }

  setUnfilteredPlatforms(vals: PRIBasicApiResponse[]) {
    this._state.unfiltered_platforms = vals.slice();
  }

  getFilteredPlatforms(): number[] {
    return(this._state.filtered_platforms.slice());
  }

  setFilteredPlatforms(vals: number[]) {
    this._state.filtered_platforms = vals.slice();
  }

  // Pagination
  getJobsDataLayoutPageSizes(): number[] {
    return(this._state.jobsdata_layout_page_sizes.slice());
  }

  getJobsDataLayoutPageSize(): number {
    return(this._state.jobsdata_layout_page_size);
  }

  setJobsDataLayoutPageSize(size: number) {
    this._state.jobsdata_layout_page_size = size;
  }

  getJobsData(): PRJobsDataResponse {
    return(JSON.parse(JSON.stringify(this._state.jobs_data)));
  }

  resetState() {
    this._state.resetAll();
  }

  getState(): PRSelectionState {
    return(JSON.parse(JSON.stringify(this._state)));
  }

  getSearchJobsHttpParams(params: PRIJobsSearchParams): {} {
    return(this._state.getSearchJobsHttpParams(params));
  }

  getCompareJobsHttpParams(params: PRIJobsComparisonParams): {} {
    return(this._state.getCompareJobsHttpParams(params));
  }
}
