
export class displayColumRule{
    canTrendData: boolean;
    customFormatter;
    colName: string;
    canSortCol: boolean;
    canBeHidden: boolean;
    canBeFiltered: boolean;
    objKey: number;
    canTrend: boolean;
    precision: number;
    sortData: string;

    constructor() {
    };

    getFormattedData(inputData: object): object {
        if( this.customFormatter !== undefined ){
            return this.customFormatter(inputData);
        }
        return inputData;
    };
};

export class JSONFormatRule{
    stripName: string = undefined;
    columnRules: object = {};

    constructor() {
    };

    addColumn(colName: string) {
        var newCol = new displayColumRule();
        newCol.colName = colName;
        this.columnRules[colName] = newCol;
    };

    getPrecision(keyName: string): number {
        for( var this_col in this.columnRules ) {
            if( this.columnRules[this_col].objKey.startsWith(keyName) ) {
                return this.columnRules[this_col].precision;
            }
        }
        return -1;
    }

    addColumns(colConfigs: object) {
        for( var colName in colConfigs ) {
            this.addColumn(colName);

            this.columnRules[colName].canSortCol = 'sortable' in colConfigs[colName] ? colConfigs[colName].sortable : false;
            this.columnRules[colName].canBeHidden = 'hideable' in colConfigs[colName] ? colConfigs[colName].hideable : true;
            this.columnRules[colName].canBeFiltered = 'filterable' in colConfigs[colName] ? colConfigs[colName].filterable : false;
            this.columnRules[colName].objKey = 'key' in colConfigs[colName] ? colConfigs[colName].key : false;
            this.columnRules[colName].canTrend = 'trendable' in colConfigs[colName] ? colConfigs[colName].trendable : true;
            this.columnRules[colName].precision = 'precision' in colConfigs[colName] ? colConfigs[colName].precision : 2;
            this.columnRules[colName].sortData = 'sortCol' in colConfigs[colName] ? colConfigs[colName].sortCol : undefined;
        };
    };

    generateUUID(): string { // Public Domain/MIT
        var d = new Date().getTime();
        if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
            d += performance.now(); //use high-precision timer if available
        }
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return String(uuid);
    }

    getColObjs(): object[] {
        var colList = [];
        for( var colName in  this.columnRules ) {
            var thisColObj = {};
            thisColObj['name'] = colName;
            thisColObj['sortable'] = this.columnRules[colName].canSortCol;
            thisColObj['hideable'] = this.columnRules[colName].canBeHidden;
            thisColObj['filterable'] = this.columnRules[colName].canBeFiltered;
            thisColObj['key'] = this.columnRules[colName].objKey;
            thisColObj['filterName'] = this.generateUUID();
            thisColObj['trendable'] = this.columnRules[colName].canTrend;
            thisColObj['sortData'] = this.columnRules[colName].sortData;
            colList.push(thisColObj);
        }
        return colList;
    };

    getRowName(inputName: string): string {
        if( this.stripName !== undefined ) {
            return inputName.replace(this.stripName, '');
        }
        else return inputName;
    };

};
