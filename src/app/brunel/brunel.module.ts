import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PRBrunelRoutingModule } from './brunel.routing.module';
import { PRSharedModule } from '../shared/shared.module';

import { PRBrunelComponent } from './brunel.component';

@NgModule({
  imports: [
    CommonModule,
    PRBrunelRoutingModule,
    PRSharedModule
  ],
  declarations: [
    PRBrunelComponent
  ],
  exports: [
    PRBrunelComponent
  ]
})
export class PRBrunelModule { }
