import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PRValidationAnalysisComponent } from './validation-analysis.component';

export const routes: Routes = [
  { path: '', component: PRValidationAnalysisComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PRValidationAnalysisRoutingModule { }
