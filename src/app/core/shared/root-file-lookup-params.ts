import { PRPlotDisplayOpts } from './plot-display-opts';

export interface PRIRootFileLookupParams {
  files: string;
  folders?: string;
  items?: string;
  compute?: PRPlotDisplayOpts;
}
