import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { PRPlotDisplayOpts } from '../../core/shared/plot-display-opts';

@Component({
  selector: 'app-plot-display-opts',
  template: `
    <div class="btn-group btn-outline-primary btn-sm">
      <div class="radio btn" *ngFor="let item of displayOpts()">
        <input type="radio"
                name="btn-group-plot-display-opts"
                id="btn-plot-display-opt-{{ item }}"
                [checked]="plotDisplayOpt === item"
                (change)="onPlotDisplayOptChanged(item)">
        <label for="btn-plot-display-opt-{{ item }}">{{ item }}</label>
      </div>
    </div>
`
})
export class PRPlotDisplayOptsComponent implements OnInit {
  @Output() public plotDisplayOptEvt: EventEmitter<PRPlotDisplayOpts> = new EventEmitter<PRPlotDisplayOpts>();
  @Input() public plotDisplayOpt: PRPlotDisplayOpts = PRPlotDisplayOpts.split;

  ngOnInit() { }

  displayOpts(): PRPlotDisplayOpts[] {
    return(Object.keys(PRPlotDisplayOpts).map(key => PRPlotDisplayOpts[key]));
  }

  onPlotDisplayOptChanged(opt: PRPlotDisplayOpts) {
    console.log('DEBUG: onPlotDisplayOptChanged: ', opt);
    this.plotDisplayOpt = opt;
    this.plotDisplayOptEvt.emit(opt);
  }
}
