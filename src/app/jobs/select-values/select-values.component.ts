import { Component, Input, Output, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { State } from '@clr/angular';

import { PRSearchJobsService } from '../../core/services/search-jobs.service';
import { PRSelectionStateService } from '../../core/services/selection-state.service';
import { PRJobsComparisonResponse } from '../../shared/classes/jobs-comparison-response';
import { PRIJobsComparisonParams } from '../../shared/interfaces/jobs-comparison-params';
import { PRMeasure } from '../../shared/classes/measure';

import { PRCompareValuesComponent } from '../compare-values/compare-values.component';

@Component({
  selector: 'app-select-values',
  templateUrl: './select-values.component.html'
})
export class PRSelectValuesComponent implements OnInit {
  @ViewChild(PRCompareValuesComponent) compareValuesChild: PRCompareValuesComponent;
  @Output() exitComparingJobsEvt: EventEmitter<boolean> = new EventEmitter<boolean>();
  visible = false;
  loading = false;
  jobsIDs: number[] = [];
  pageSizes: number[];
  pageSize = 10;
  comparedJobsData: PRJobsComparisonResponse;
  selectedItems: PRMeasure;

  constructor(private _jobs: PRSearchJobsService, private _state: PRSelectionStateService) { }

  ngOnInit() {
    this.pageSizes = this._state.getJobsDataLayoutPageSizes();
    this.pageSize = this.pageSizes[0];
  }

  onRefreshDatagrid(state?: State) {
    if (this.jobsIDs.length) {
      // This timeout is required due to a bug in ClrDatagrid
      setTimeout(() => { this.loading = true; });

      const params: PRIJobsComparisonParams = {
        'ids': this.jobsIDs.slice(),
        'pagesize': this.pageSize
      };

      if (state) {
        if (state.page) {
          params.page = (state.page.from) ? (state.page.from / state.page.size) + 1 : 1;
        }
        if (state.filters) {
          params.filterby = state.filters[0]['value'];
        }
      }

      this._jobs.getCompareJobs(this._state.getCompareJobsHttpParams(params))
          .subscribe((res) => {
            this.comparedJobsData = res;
            this.loading = false;
          });
    }
  }

  onPageSizeChange(event: {type: string, value: number}) {
    console.log(event);
    if (event.value) {
      this.pageSize = Number(event.value);
      this.onRefreshDatagrid();
    }
  }

  onExitComparingJobs() {
    this.jobsIDs = [];
    this.comparedJobsData.results = [];
    this.exitComparingJobsEvt.emit(true);
  }

  plot() {
    // console.log(this._selectedItems);
    this.compareValuesChild.data = this.selectedItems;
    this.compareValuesChild.plot();
    this.compareValuesChild.modalOpened = true;
  }
}
