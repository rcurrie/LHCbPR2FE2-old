import { Component, OnInit, ViewChild } from '@angular/core';

import { JSONTableService } from '../json-table/json-table.component';
import { JSONFormatRule } from '../json-table/json-format.component';
import { JSONQueryService } from '../../core/services/json-query/json-query.service';
import { SingleJobSelectorState } from '../../core/services/json-query/single-job-selector-state';

export class FilterButton {
    'name': string='';
    'class': string='btn btn-primary';
    'active': boolean=false;
    'contains': string[]=[];
    'not_contains': string[]=[];
    'columns': JSONFormatRule;
};

@Component({
  selector: 'app-json-filter',
  templateUrl: './json-filter-table.component.html',
  providers: [  JSONTableService,
              ],
})
export class JSONFilterTable implements OnInit {

  users = [];
  value = {};
  value2 = {};
  testParam: string;
  @ViewChild('tableData') _tableService: JSONTableService;
  public jobId: number=0;
  mooreName: string="MOORE";
  hideFilter: boolean=true;

  filter_buttons = [];

  constructor(private _queryService: JSONQueryService) {
  };

  ngOnInit() {
  };

  setUpTable(buttons: FilterButton[]) {
    this.filter_buttons = buttons;
  };

  jobChanged(newJobID: number, selectionState: SingleJobSelectorState ) {
    for(var i=0; i< this.filter_buttons.length; i++) {
        this.filter_buttons[i].class = 'btn btn-primary';
        this.filter_buttons[i].active = false;
    }

    this._tableService.clearTable();

    this.jobId = newJobID;
    this._tableService.inputJob = this.jobId;
    this._tableService.selectorState = selectionState;
    if(newJobID) {
        this.hideFilter = false;
    } else {
        this.hideFilter = true;
        for(var i=0; i< this.filter_buttons.length; i++) {
            this.filter_buttons[i].class = 'btn btn-primary';
            this.filter_buttons[i].active = false;
        }
    }
  };

  toggleButton(buttonNum: number) {
    if( this.filter_buttons[buttonNum].class == 'btn btn-primary') {
        this.filter_buttons[buttonNum].class = 'btn btn-success';
        this.filter_buttons[buttonNum].active = true;
        this._tableService.setupTable(this.filter_buttons[buttonNum].columns);
    } else {
        this.filter_buttons[buttonNum].class = 'btn btn-primary';
        this.filter_buttons[buttonNum].active = false;
    }
    for(var i=0; i< this.filter_buttons.length; i++) {
        if( i == buttonNum ) continue;
        this.filter_buttons[i].class = 'btn btn-primary';
        this.filter_buttons[i].active = false;
    }
  };

  clickButton(buttonName: string) {
    console.log("Clicked " + buttonName);
    for(var i=0; i< this.filter_buttons.length; i++) {
        if( this.filter_buttons[i].name == buttonName ) {
            this.toggleButton(i);

            if( this.filter_buttons[i].active ) {
                this._tableService.filter = [this.filter_buttons[i].contains, this.filter_buttons[i].not_contains];
                this._tableService.formattingRules.stripName = this.filter_buttons[i].contains[0];
            } else {
                this._tableService.clearTable();
            }
            break;
        }
    }
  };

};
