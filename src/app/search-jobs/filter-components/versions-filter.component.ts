import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { PRIVersionsApiResponse } from '../../shared/interfaces/versions-api-response';
import { PRISelectionEvent } from '../../shared/interfaces/selection-event';

@Component({
  selector: 'app-versions-filter',
  templateUrl: 'versions-filter.component.html'
})
export class PRVesionsFilterComponent {
  @Input() items: PRIVersionsApiResponse[] = [];
  @Input() filteredItems: number[] = [];
  @Input() group: FormGroup;
  @Output() selectionChanged: EventEmitter<PRISelectionEvent> = new EventEmitter<PRISelectionEvent>();

  constructor() {
    this.group = new FormGroup({
      toggle_show_versions: new FormControl(),
      number_of_versions: new FormControl()
    });
  }

  private _onToggleShowVersions() {
    if (this.filteredItems.length) {
      this.filteredItems.length = 0;
      this._emittEvent();
    }
  }

  private _onSelectionChange(id: number, isSelected: boolean) {
    if (isSelected && this.filteredItems.indexOf(id, 0) === -1) {
      this.filteredItems.push(id);
      this._emittEvent();
    } else if (!isSelected) {
      const index = this.filteredItems.indexOf(id, 0);
      if (index !== -1) {
        this.filteredItems.splice(index, 1);
        this._emittEvent();
      }
    }
  }

  private _emittEvent() {
    this.selectionChanged.emit({
      type: 'versions',
      data: this.filteredItems.slice()
    });
  }
}
