
import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs/Rx';
import { PR_API_MAP } from '../../../shared/api-map';
import { AllJobResults, JSONTrendData, UniqueBuildIDs, FilteredOptions, FilteredPlatforms, JobsIDs, perfData, jobData, justJobData } from './json-query-interface';

import { SingleJobSelectorState } from './single-job-selector-state';

@Injectable()
export class JSONQueryService{

  @Input() appName: string = "";

  constructor(private _http: HttpClient) {
  };

  getAllJSONResults(job_id: number, contains: string[]=[], not_contains: string[]=[]): Observable<AllJobResults> {
    var args = 'id=' + job_id;
    args = args + '&contains=' + contains.join(':');
    args = args + '&not_contains=' + not_contains.join(':');
    return this._http.get<AllJobResults>(`${PR_API_MAP.url_alljsonresults}?`+args);
  };

  getJSONTrendData(param_name: string, result_name: string, app_version: string, limit: number=21, otherArgs: string[]): Observable<JSONTrendData> {
    /* https://lblhcbpr.cern.ch/api/jsontrends/?app_versions=lhcb-2018-patches&callback=angular.callbacks._4&format=jsonp&limit=21&names=hlt2_decisions_Total&options=RateTest_0x1707&params=rate
    */
    var args ='limit=21&names=' + result_name + '&params=' + param_name;
    args = args + '&app_versions=' + app_version;
    for(var i=0; i< otherArgs.length; i++) {
      args = args + '&' + otherArgs[i];
    }
    return this._http.get<JSONTrendData>(`${PR_API_MAP.url_jsontrends}?`+args);
  };

  _getOption(param: string, option: string): string {
    if( option.startsWith('!') == true ) {
      var output = param + "!=" + option.slice(1);
    } else {
      var output = param + "=" + option;
    }
    return output;
  }

  _getTheseArgs(selectionState: SingleJobSelectorState): string {
    var args = this._getOption('application', selectionState.app_id);
    if( selectionState.build_id ) args = args + '&' + this._getOption('app_versions', selectionState.build_id);
    if( selectionState.option_id ) args = args + '&' + this._getOption('options', selectionState.option_id);
    if( selectionState.platform_id ) args = args + '&' + this._getOption('platforms', selectionState.platform_id);
    if( selectionState.exec_id ) args = args + '&' + this._getOption('executables', selectionState.exec_id);
    args = args + "&page_size=1000";
    return args;
  };

  getBuildIDs(selectionState: SingleJobSelectorState): Observable<UniqueBuildIDs> {
    var args = this._getTheseArgs(selectionState);
    return this._http.get<UniqueBuildIDs>(`${PR_API_MAP.url_unique_build_ids}?`+args);
  };

  getFilteredOptions(selectionState: SingleJobSelectorState): Observable<FilteredOptions> {
    var args = this._getTheseArgs(selectionState);
    return this._http.get<FilteredOptions>(`${PR_API_MAP.url_filteredoptions}?`+args);
  };

  getFilteredPlatforms(selectionState: SingleJobSelectorState): Observable<FilteredPlatforms> {
    var args = this._getTheseArgs(selectionState);
    return this._http.get<FilteredPlatforms>(`${PR_API_MAP.url_filteredplatforms}?`+args);
  };

  getJobIds(selectionState: SingleJobSelectorState): Observable<JobsIDs> {
    var args = this._getTheseArgs(selectionState);
    args = args + '&sortby=date';
    return this._http.get<JobsIDs>(`${PR_API_MAP["url_job-ids"]}?`+args);
  };

  // Test for the moment, may possibly develop something json based which doesn't
  // require passing so much data through the API all the time
  getPerfData(job_id: string="44910", page: number = 1, attr: number = undefined): Observable<perfData>{
    //https://lblhcbpr.cern.ch/api/compare/?format=jsonp&ids=43280&page_size=10000
    var args = 'ids='+String(job_id)+'&page_size=100&page=' + String(page);
    if( attr !== undefined ) {
      args = args + '&attrs='+String(attr);
    }
    return this._http.get<perfData>(`${PR_API_MAP.url_compare}?`+args);
  };


  // API for getting a more compact set of data back just containing the data collected by the running job
  getRawData(job_id: string, attr: string = undefined): Observable<justJobData>{
    var args = 'ids='+String(job_id);
    if( attr !== undefined ) {
      args = args + '&contains='+String(attr);
    }
    return this._http.get<justJobData>(`${PR_API_MAP.url_jobdata}?`+args);
  };


  // Hack to work around pagination
  getAllPerfData(job_id: string="44910", progressObserver: Observer<number> | undefined = undefined): Observable<perfData> {
    var page = 1;
    return Observable.create(observer => {
      this.getPerfData(job_id, page)
      // Expand allows us to keep making new queries and return the data
        .expand(data => {
            page++;
            if(data.next) {
              if(progressObserver)
                progressObserver.next((page * 100 / data.count) * 100);
              return this.getPerfData(job_id, page);
            } else {
              if(progressObserver) {
                progressObserver.next(100);
                progressObserver.complete();
              }
              return Observable.empty();
            }
        })
      // Reduce to consolidate all of  
        .reduce((acc, data) => {
          acc.count = data.count;
          acc.results = acc.results.concat(data.results.slice());
          return acc;
        })
      // Subscribe to make sure everything works
        .subscribe((finalData) => {
          observer.next(finalData);
          observer.complete();
        });
      });
  };

  getJobData(job_id: number): Observable<jobData> {
    return this._http.get<jobData>(`${PR_API_MAP.url_jobs}`+job_id);
  };

  getJobsFromDate(application: string, app_versions: string, app_options: string, date: string): Observable<JobsIDs> {
    var args = '?requested_date='+date;
    args = args + '&application='+application;
    args = args + '&app_versions='+app_versions;
    args = args + '&options='+app_options;
    return this._http.get<JobsIDs>(`${PR_API_MAP.url_jobsbydate}`+args);
  };

};
