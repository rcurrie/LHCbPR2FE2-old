import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PRJobsComponent } from './jobs.component';

export const routes: Routes = [
  { path: 'details', component: PRJobsComponent },
  { path: 'viewer', loadChildren: './jobs-file-viewer/jobs-file-viewer.module#PRJobsFileViewerModule' },
  { path: '', redirectTo: 'details', pathMatch: 'full' },
  { path: '**', redirectTo: 'details' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PRJobsRoutingModule { }
