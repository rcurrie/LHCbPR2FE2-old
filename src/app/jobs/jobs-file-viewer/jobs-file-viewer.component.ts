import { Component, OnInit } from '@angular/core';

import { PRSearchJobsService } from '../../core/services/search-jobs.service';
import { PRSelectionStateService } from '../../core/services/selection-state.service';

@Component({
  selector: 'app-jobs-file-viewer',
  templateUrl: './jobs-file-viewer.component.html'
})
export class PRJobsFileViewerComponent implements OnInit {

  constructor(private _jobs: PRSearchJobsService,
              private _state: PRSelectionStateService) { }

  ngOnInit() { }
}
