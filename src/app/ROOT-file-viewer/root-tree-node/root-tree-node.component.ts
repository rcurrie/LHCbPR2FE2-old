import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root-tree-node',
  template: `
    <clr-tree-node [(clrSelected)]="item.selected">
      {{ item.label }}
      <ng-container *ngIf="item.children?.length" ngProjectAs="[clrIfExpanded]">
        <ng-template [(clrIfExpanded)]="item.expanded" *ngFor="let child of item.children">
          <app-root-tree-node [item]="child"></app-root-tree-node>
        </ng-template>
      </ng-container>
      <ng-template *ngIf="item && item.children && !item.children[0]" [(clrIfExpanded)]="item.children.expanded">
        <app-root-tree-node [item]="item.children"></app-root-tree-node>
      </ng-template>
    </clr-tree-node>
`
})
export class PRROOTTreeNodeComponent {
  @Input() public item: any;
  @Input() public selected = false;
}
