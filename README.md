# LHCbPR2FE2


## Quick HOWTO

### Install Node

Install `node` and `npm` on your system, (alternatively download them from: https://nodejs.org/en/ https://www.npmjs.com/get-npm )

#### For most systems

`dnf install nodejs npm`

`zypper install nodejs npm`

`apt-get install nodejs npm`


### Clone the environment and setup what is needed

#### Get the code
Git clone this repo:

`git clone https://gitlab.cern.ch/dpopov/LHCbPR2FE2 ; cd LHCbPR2FE2`

#### Setup the env

Install the dependencies:

`npm install .`

### Launch the service

To run the npm-cli from this projct to host a local service:

`node ./node_modules/.bin/ng serve --aot`

(I've also found node8 explicitly works but ymmv)

## Development server
Run `ng serve --aot` for a dev server and navigate to `http://localhost:4200/`.

## Build
Run `ng build`, `ng build --prod` or `ng build --prod --build-optimizer` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io/).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

