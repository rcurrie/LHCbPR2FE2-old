import { Component, OnInit } from '@angular/core';

import { PRJobFiltersPresets } from '../../shared/classes/job-filters-presets';

@Component({
  selector: 'app-validation-analysis',
  templateUrl: './validation-analysis.component.html'
})
export class PRValidationAnalysisComponent implements OnInit {
  public searchJobsFilters: PRJobFiltersPresets;

  constructor() { }

  ngOnInit() {
    // Preset application to Gauss and options to Validation Analysis
    this.searchJobsFilters = new PRJobFiltersPresets(
      4 // App ID for Gauss
      // [5, 13, 14, 15, 16, 17, 18, 19, 25, 26, 27, 29, 30, 51, 52, 53] // Options IDs
    );
  }
}
