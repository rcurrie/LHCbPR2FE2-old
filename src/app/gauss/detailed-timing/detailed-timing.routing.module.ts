import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PRDetailedTimingComponent } from './detailed-timing.component';

export const routes: Routes = [
  { path: '', component: PRDetailedTimingComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PRDetailedTimingRoutingModule { }
