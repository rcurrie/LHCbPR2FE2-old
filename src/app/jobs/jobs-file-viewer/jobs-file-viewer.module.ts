import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClarityModule } from '@clr/angular';

import { PRJobsFileViewerRoutingModule } from './jobs-file-viewer.routing.module';
import { PRSharedModule } from '../../shared/shared.module';
import { PRROOTFileViewerModule } from '../../ROOT-file-viewer/ROOT-file-viewer.module';

import { PRJobsFileViewerComponent } from './jobs-file-viewer.component';

@NgModule({
  imports: [
    CommonModule,
    ClarityModule,
    PRJobsFileViewerRoutingModule,
    PRSharedModule,
    PRROOTFileViewerModule
  ],
  declarations: [
    PRJobsFileViewerComponent
  ],
  exports: [
    PRJobsFileViewerComponent
  ]
})
export class PRJobsFileViewerModule { }
