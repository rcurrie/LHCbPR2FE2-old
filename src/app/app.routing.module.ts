import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  { path: 'jobs', loadChildren: 'app/jobs/jobs.module#PRJobsModule' },
  { path: 'gauss', loadChildren: 'app/gauss/gauss.module#PRGaussModule' },
  { path: 'geant', loadChildren: 'app/geant/geant.module#PRGeantModule' },
  { path: 'hlt', loadChildren: 'app/hlt/hlt.module#PRHLTModule' },
  { path: 'brunel', loadChildren: 'app/brunel/brunel.module#PRBrunelModule' },
  { path: 'faq', loadChildren: 'app/faq/faq.module#PRFAQModule' },
  { path: '', redirectTo: 'jobs', pathMatch: 'full' },
  { path: '**', redirectTo: 'jobs' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PRAppRoutingModule { }
