import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClarityModule } from '@clr/angular';

import { PRSharedModule } from '../shared/shared.module';
import { PRSearchJobsModule } from '../search-jobs/search-jobs.module';

import { PRROOTFileViewerComponent } from './ROOT-file-viewer.component';
import { PRROOTTreeNodeComponent } from './root-tree-node/root-tree-node.component';
import { PRPlotDisplayOptsComponent } from './plot-display-opts/plot-display-opts.component';
import { PRJSROOTPlotComponent } from './jsroot-plot-component/jsroot-plot-component';

@NgModule({
  imports: [
    CommonModule,
    ClarityModule,
    PRSharedModule,
    PRSearchJobsModule
  ],
  declarations: [
    PRROOTFileViewerComponent,
    PRROOTTreeNodeComponent,
    PRPlotDisplayOptsComponent,
    PRJSROOTPlotComponent
  ],
  exports: [
    PRROOTFileViewerComponent
  ]
})
export class PRROOTFileViewerModule { }
