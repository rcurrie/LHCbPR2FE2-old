import { Component, Input, ViewChild, OnInit } from '@angular/core';

import { PRSearchJobsService } from '../core/services/search-jobs.service';
import { PRSelectionStateService } from '../core/services/selection-state.service';
import { PRRootFilesLookupService } from '../core/services/root-files-lookup.service';

import { PRPlotDisplayOpts } from '../core/shared/plot-display-opts';
import { PRIRootFileLookupResponse } from '../core/shared/root-file-lookup-response';
import { PRISelectionEvent } from '../shared/interfaces/selection-event';

import { PRJobFiltersPresets } from '../shared/classes/job-filters-presets';

import { PRSelectJobsComponent } from '../search-jobs/select-jobs/select-jobs.component';

import { forEach, has } from 'lodash';

@Component({
  selector: 'app-root-file-viewer',
  templateUrl: './ROOT-file-viewer.component.html'
})
export class PRROOTFileViewerComponent implements OnInit {
  @ViewChild(PRSelectJobsComponent) private selectJobsChild: PRSelectJobsComponent;
  @Input() public searchJobsFilters: PRJobFiltersPresets = new PRJobFiltersPresets();
  public tree_view: any[] = [];
  public selected_plots: any[] = [];
  public jsroot_plots: any[] = [];

  public tabsHidden = true;
  public filetreeTabActive = true;
  public plotsTabActive = false;
  public plotDisplayOpt = PRPlotDisplayOpts.split;

  constructor(private _jobs: PRSearchJobsService,
              private _state: PRSelectionStateService,
              private _root_lookup: PRRootFilesLookupService) { }

  ngOnInit() { }

  // Triggered by: SearchJobs when selection criteria change
  // Triggers: SelectJobs to refresh the list of jobs
  public onSearchSelectionChanged() {
    this.selectJobsChild.selectedItems = [];
    this.onReturnToSearch();
    this.selectJobsChild.refreshDatagrid();
  }

  // Triggered by: SelectJobs when 'Analyse' button is clicked
  // Triggers: SelectJobs and SelectValues
  public onAnalyseJobs(payload?: PRISelectionEvent) {
    if (payload.data.length) {
      this._root_lookup.getTreeView(payload.data.slice())
          .subscribe(
            (data) => { this.tree_view = data; },
            (error) => {},
            () => {
              // Hide the jobs search data-grid
              this.selectJobsChild.visible = false;
              this.tabsHidden = false;
            }
          );
    }
  }

  // Hide and clear the plots and show the search results grid
  public onReturnToSearch() {
    this.tabsHidden = true;
    setTimeout(() => {
      this.plotsTabActive = false;
      this.filetreeTabActive = true;
    });

    // Clear the local state
    this.selected_plots = [];
    this.jsroot_plots = [];
    this.tree_view = [];

    // Show search results
    this.selectJobsChild.visible = true;
  }

  private _selectCheckedTreeItems(items: any) {
    forEach(items, item => {
      if (!item.children && item.selected) {
        this.selected_plots.push(item);
      } else if (item.children) {
        this._selectCheckedTreeItems(item.children);
      }
    });
  }

  public onDrawPlots() {
    // Clear old selection
    this.selected_plots = [];
    this.jsroot_plots = [];

    this._selectCheckedTreeItems(this.tree_view);

    this._root_lookup.getPlots(this.selected_plots, this.plotDisplayOpt)
        .subscribe((res: PRIRootFileLookupResponse[]) => {
          // Loop over response-per-plot
          forEach(res, (item) => {
            const plots_per_file = [];
            forEach(item.result, (file) => {
              const plot_item = {};
              if (has(file, 'root')) {
                plot_item['job_id'] = file.root.split('/')[0];
                forEach(file.items, (plot_data, plot_name) => {
                  plot_item['plot_title'] = plot_name;
                  plot_item['plot_data'] = plot_data;
                });
              }
              if (has(file, 'computed_result')) {
                plot_item['plot_title'] = String(this.plotDisplayOpt);
                plot_item['plot_data'] = file.computed_result;
              }
              if (has(file, 'KSTest')) {
                plot_item['plot_title'] = String(this.plotDisplayOpt);
                plot_item['plot_data'] = file.KSTest;
              }
              plots_per_file.push(plot_item);
            });
            this.jsroot_plots.push(plots_per_file);
          });
        });

    // Activate Plots tab
    setTimeout(() => {
      this.filetreeTabActive = false;
      this.plotsTabActive = true;
    });
  }

  onPlotDisplayOpt(display_opt: PRPlotDisplayOpts) {
    this.plotDisplayOpt = display_opt;
    this.onDrawPlots();
  }
}
