import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PRBrunelComponent } from './brunel.component';

export const routes: Routes = [
  { path: 'minibrunel', component: PRBrunelComponent },
  { path: '', redirectTo: 'minibrunel', pathMatch: 'full' },
  { path: '**', redirectTo: 'minibrunel' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PRBrunelRoutingModule { }
