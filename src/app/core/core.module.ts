import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { throwIfAlreadyLoaded } from './module-import-guard';
import { PRSearchJobsService } from './services/search-jobs.service';
import { JSONQueryService } from './services/json-query/json-query.service';
import { PRSelectionStateService } from './services/selection-state.service';

import { ClarityIcons } from '@clr/icons';
import { ClrShapeObjects } from '@clr/icons/shapes/essential-shapes';
import { ClrShapeUndo } from '@clr/icons/shapes/essential-shapes';
import { ClrShapeLineChart } from '@clr/icons/shapes/chart-shapes';

ClarityIcons.add({
  objects: ClrShapeObjects,
  undo: ClrShapeUndo,
  linechart: ClrShapeLineChart
});

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    CommonModule
  ],
  providers: [
    JSONQueryService,
    PRSearchJobsService,
    PRSelectionStateService
  ]
})
export class PRCoreModule {
  constructor(@Optional() @SkipSelf() parentModule: PRCoreModule) {
    throwIfAlreadyLoaded(parentModule, 'PRCoreModule');
  }
}
