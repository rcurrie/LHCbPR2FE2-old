import { PRJobValue } from './jobvalue';

export class PRMeasure {
  'id': number;
  'name': string;
  'dtype': string;
  'description': string;
  'thresholds': any[];
  'jobvalues': PRJobValue[];
}
