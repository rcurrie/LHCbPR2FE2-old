export interface PRIVersionsApiResponse {
  count?: number;
  values: [
    {
      id: number;
      name: string;
      count: number;
    }
  ];
  name: string;
}
