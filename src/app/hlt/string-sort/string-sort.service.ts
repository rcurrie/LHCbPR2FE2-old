import { Injectable } from '@angular/core';

@Injectable()
export class StringSortService{

    reA = /[^a-zA-Z]/g;
    reN = /[^0-9]/g;

    sortAlphaNum(a: any, b: any): number {

        var AInt = parseInt(a, 10);
        var BInt = parseInt(b, 10);

        if(isNaN(AInt) && isNaN(BInt)){
            return a.localeCompare(b);
            /*
            var aA = a.replace(this.reA, "");
            var bA = b.replace(this.reA, "");
            if(aA === bA) {
                var aN = parseInt(a.replace(this.reN, ""), 10);
                var bN = parseInt(b.replace(this.reN, ""), 10);
                return aN === bN ? 0 : aN > bN ? 1 : -1;
            } else {
                return aA > bA ? 1 : -1;
            }*/
        } else if(isNaN(AInt)){//A is not an Int
            return 1;//to make alphanumeric sort first return -1 here
        } else if(isNaN(BInt)){//B is not an Int
            return -1;//to make alphanumeric sort first return 1 here
        }else{
            return AInt > BInt ? 1 : -1;
        }
    }

    reverse(result: number): number {
        if( result == 1 ) {
            return -1;
        } else if( result == -1 ) {
            return 1;
        } else return result;
    }

    reverseSortAlpaNum(a: any, b: any): number {
        return this.reverse(this.sortAlphaNum(a, b));
    }

    sortByAttribute(a: object, b: object, c: string): number {
        if(c == '' || c === undefined) {
            return 0;
        } else if(c in a && c in b) {
            return this.sortAlphaNum(a[c], b[c]);
        } else return 0;
    }

    reverseSortByAttribute(a: object, b: object, c: string): number {
        return this.reverse(this.sortByAttribute(a, b, c));
    }

}