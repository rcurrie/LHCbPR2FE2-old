import { Component, Input, ViewChild } from '@angular/core';
import { PlotlyChartData } from '../../shared/components/plotly-chart/plotly-chart.component';

@Component({
  selector: 'app-compare-values',
  templateUrl: './compare-values.component.html'
})
export class HLTCompareValuesComponent {
  @Input() public title: string;
  @Input() public data: number[];
  @Input() public error: number[];
  @Input() public labels: string[];
  @Input() public modalOpened: boolean = false;

  @ViewChild('plottingArea') plottable: PlotlyChartData;
  @ViewChild('modalObj') modal: any;

  plot() {
    var original = this.modal.fadeDone;
    this.modal.fadeDone = function(e: AnimationEvent) {
      this.plottable.title = this.title;
      this.plottable.data = this.data;
      this.plottable.error = this.error;
      this.plottable.labels = this.labels;
      this.plottable.plot();
      this.modal.fadeDone = original;
      original(e);
    }.bind(this);
    this.modalOpened = true;
  }

};
