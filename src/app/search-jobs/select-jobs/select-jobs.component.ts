import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { State } from '@clr/angular';

import { PRSelectionState } from '../../shared/classes/selection-state';
import { PRISelectionEvent } from '../../shared/interfaces/selection-event';
import { PRJobsDataResponse } from '../../shared/classes/jobs-data-response';
import { PRSelectionStateService } from '../../core/services/selection-state.service';
import { PRSearchJobsService } from '../../core/services/search-jobs.service';
import { PRIJobsSearchParams } from '../../shared/interfaces/jobs-search-params';

@Component({
  selector: 'app-select-jobs',
  templateUrl: './select-jobs.component.html'
})
export class PRSelectJobsComponent implements OnInit {
  @Input() visible = true;
  @Output() analyseJobsEvt: EventEmitter<PRISelectionEvent> = new EventEmitter<PRISelectionEvent>();
  selectedItems: number[] = [];
  pageSizes: number[];
  loading = false;
  pageSize = 10;
  jobsData: PRJobsDataResponse;

  constructor(private _jobs: PRSearchJobsService, private _state: PRSelectionStateService) { }

  ngOnInit() {
    this.pageSizes = this._state.getJobsDataLayoutPageSizes();
    this.pageSize = this.pageSizes[0];
  }

  refreshDatagrid(state?: State) {
    if (this._state.getAppID()) {
      // This timeout is required due to a bug in ClrDatagrid
      setTimeout(() => { this.loading = true; });

      const params: PRIJobsSearchParams = {
        'pagesize': this.pageSize
      };

      if (state && state.page) {
        params.page = (state.page.from) ? (state.page.from / state.page.size) + 1 : 1;
      }

      this._jobs.getJobsData(this._state.getSearchJobsHttpParams(params))
        .subscribe((res) => {
          this.jobsData = res;
          this.loading = false;
        });
    }
  }

  onPageSizeChange(event: {type: string, value: number}) {
    if (event.value) {
      this.pageSize = Number(event.value);
      this.refreshDatagrid();
    }
  }

  onAnalyseJobs() {
    this.analyseJobsEvt.emit({
      type: 'jobs',
      data: this.selectedItems.slice()
    });
  }
}
